<?php

use Faker\Generator as Faker;
use App\Models\Teacher;

$factory->define(App\Models\Teacher::class, function (Faker $faker) {
    return [
        'nama' => $faker->name,
    ];
});
