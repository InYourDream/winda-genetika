<?php

use Illuminate\Database\Seeder;

class SubjectGradeTeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grades = Grade::all();

        $x_mipa = $grades->where('class_level_id', 1)->pluck('id')->toArray();
        $x_ips = $grades->where('class_level_id', 2)->pluck('id')->toArray();
        $x = array_merge($x_mipa, $x_ips);

        $xi_mipa = $grades->where('class_level_id', 3)->pluck('id')->toArray();
        $xi_ips = $grades->where('class_level_id', 4)->pluck('id')->toArray();
        $xi = array_merge($xi_mipa, $xi_ips);

        $xii_mipa = $grades->where('class_level_id', 5)->pluck('id')->toArray();
        $xii_ips = $grades->where('class_level_id', 6)->pluck('id')->toArray();
        $xii = array_merge($xii_mipa, $xii_ips);

        $data =  [
            ['kode' => '2b', 'teacher_id' => '2', 'nama' => 'Suharjo, S.Pd', 'subject_id' => '1', 'grade_id' => $x],
            ['kode' => '3b', 'teacher_id' => '3', 'nama' => 'Triyono, S.Pd', 'subject_id' => '1', 'grade_id' => $xii],
            ['kode' => '6a', 'teacher_id' => '6', 'nama' => 'Sumaryanti, S.Pd, Ek', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '7 ', 'nama' => 'Bernadeta S, S.Ag', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '8', 'nama' => 'Antonius Tukimin, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '9', 'nama' => 'Yuyuniarti, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '10', 'nama' => 'Hesti Pamuji R, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '11', 'nama' => 'Mimi Susanti, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '12', 'nama' => 'Suratmi, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '13', 'nama' => 'Cucu Dariah, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '14', 'nama' => 'Kristologus Bai, S.Ag', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '15', 'nama' => 'Edwin H. Simanjuntak, SP', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '16', 'nama' => 'Mastiah Hasanah, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '17', 'nama' => 'Ria Asti Nugroho, SS', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '18', 'nama' => 'Darmawansyah, S.H.I', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '19', 'nama' => 'Yanti Indrawati, SP', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '20', 'nama' => 'Norita Diana, S.Hut', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '21', 'nama' => 'Fitriana Octaviani, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '22', 'nama' => 'Nuryanti, S.Si', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '23', 'nama' => 'Rohani Yulianti,S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '24', 'nama' => 'Dewi Agustiyarni, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '25', 'nama' => 'Suryati, S.Th', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '26', 'nama' => 'Nyemas Sari Wulan Aprilia,S.Pd.', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '27', 'nama' => 'Dina Kurniawati, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '28', 'nama' => 'Elisabet, S.Sos', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '29', 'nama' => 'Djarobi, BA', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '30', 'nama' => 'Welan', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '31', 'nama' => 'Firdaus, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '32', 'nama' => 'Z. Asmarady, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '33', 'nama' => 'Anang Purwanto, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '34', 'nama' => 'Wiwi Pitria, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '35', 'nama' => 'Johan Kurniawan Budi, S.Sn', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '36', 'nama' => 'Viktorianus Toding, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '37', 'nama' => 'Sudarmi, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '38', 'nama' => 'Marina Kurniasih S, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '39', 'nama' => 'Fransiskus Andut, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '40', 'nama' => 'Trilis Hartati,S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '41', 'nama' => 'Rona Ramadhan, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '42', 'nama' => 'Saripa Manurung, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '43', 'nama' => 'Erli Marlida, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '44', 'nama' => 'Kuswanda, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '45', 'nama' => 'Nurhidayati, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '46', 'nama' => 'Dea Aries Fitriani,S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '47', 'nama' => 'Sodikun, S.Sos', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '48', 'nama' => 'Kristina', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '51', 'nama' => 'Edwar Rusliman, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '52', 'nama' => 'Melyana Dewi,S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '53', 'nama' => 'Rahmi Sapariyanti,S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '54', 'nama' => 'Hasmuniasyah,S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '55', 'nama' => 'Apolonius Suhardi,S.Ag', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '56', 'nama' => 'Fatima Tuzahrah,S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '57', 'nama' => 'Rendy Suwarno, S.Pd', 'subject_id' => '', 'grade_id' => $x],
            ['kode' => '2a', 'teacher_id' => '58', 'nama' => 'Heni, S.Pd', 'subject_id' => '', 'grade_id' => $x],
        ];
    }
}
