<?php

use Illuminate\Database\Seeder;
use App\Models\ClassLevel;

class ClassLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $ClassLevels = [
            ['nama' => 'X MIPA', 'kelas' => 'X', 'major_id' => '1'],
            ['nama' => 'X IPS', 'kelas' => 'X', 'major_id' => '2'],
            ['nama' => 'XI MIPA', 'kelas' => 'XI', 'major_id' => '1'],
            ['nama' => 'XI IPS', 'kelas' => 'XI', 'major_id' => '2'],
            ['nama' => 'XII MIPA', 'kelas' => 'XII','major_id' => '1'],
            ['nama' => 'XII IPS', 'kelas' => 'XII', 'major_id' => '2'],
        ];

        foreach ($ClassLevels as $classLevel) {
            ClassLevel::create($classLevel);
        }

    }
}
