<?php

use Illuminate\Database\Seeder;
use App\Models\Grade;
use App\Models\ClassLevel;

class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('grades')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $classLevels = ClassLevel::all();
        $indexes = ['1', '2', '3', '4'];

        foreach ($classLevels as $classLevel) {
            foreach ($indexes as $index) {
                Grade::create([
                    'nama' => "$classLevel->nama $index",
                    'class_level_id' => $classLevel->id,
                ]);
            }
        }
    }
}
