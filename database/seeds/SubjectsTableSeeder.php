<?php

use Illuminate\Database\Seeder;
use App\Models\Subject;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'nama' => 'Penjasorkes', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [1, 2, 3, 4, 5, 6]],
            ['id' => 2, 'nama' => 'Pend. Agama', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [1, 2, 3, 4, 5, 6]],
            ['id' => 3, 'nama' => 'PPKn', 'jam' => '2',  'total_jam_pelajaran' => 2, 'pertemuan' => 1, 'classes' => [1, 2, 3, 4, 5, 6]],
            ['id' => 4, 'nama' => 'Seni Budaya', 'jam' => '2',  'total_jam_pelajaran' => 2, 'pertemuan' => 1, 'classes' => [1, 2, 3, 4, 5, 6]],
            ['id' => 5, 'nama' => 'Prakarya dan Kewr', 'jam' => '2',  'total_jam_pelajaran' => 2, 'pertemuan' => 1, 'classes' => [1, 2, 3, 4, 5, 6]],
            ['id' => 6, 'nama' => 'Sejarah Indonesia', 'jam' => '2',  'total_jam_pelajaran' => 2, 'pertemuan' => 1, 'classes' => [1, 2, 3, 4, 5, 6]],

            ['id' => 7, 'nama' => 'Ekonomi Akuntansi 10 (LM)', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [1]],
            ['id' => 8, 'nama' => 'Ekonomi Akuntansi 10', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [2]],
            ['id' => 9, 'nama' => 'Ekonomi Akuntansi 11 & 12 (LM)', 'jam' => '2 2',  'total_jam_pelajaran' => 4, 'pertemuan' => 2, 'classes' => [3, 5]],
            ['id' => 10, 'nama' => 'Ekonomi Akuntansi 11 & 12', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [4, 6]],

            ['id' => 11, 'nama' => 'Bahasa Indonesia', 'jam' => '2 2',  'total_jam_pelajaran' => 4, 'pertemuan' => 2, 'classes' => [1, 2, 3, 4, 5, 6]],
            ['id' => 12, 'nama' => 'Bahasa Inggris', 'jam' => '2',  'total_jam_pelajaran' => 2, 'pertemuan' => 2, 'classes' => [1, 2, 3, 4, 5, 6]],
            ['id' => 13, 'nama' => 'Bahasa Inggris (LM)', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 3, 'classes' => [1, 2]],

            ['id' => 14, 'nama' => 'Matematika Wajib', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [1, 2, 3, 4, 5 ,6]],
            ['id' => 15, 'nama' => 'Matematika Peminatan 10', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [1]],
            ['id' => 16, 'nama' => 'Matematika Peminatan 11 & 12', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [3, 5]],

            ['id' => 17, 'nama' => 'Biologi IPA 10', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [1]],
            ['id' => 18, 'nama' => 'Biologi IPA 11 & 12', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [3, 5]],
            ['id' => 19, 'nama' => 'Biologi 10 (LM)', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [2]],
            ['id' => 20, 'nama' => 'Biologi 11 & 12 (LM)', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [4, 6]],

            ['id' => 21, 'nama' => 'Kimia 10', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [1]],
            ['id' => 22, 'nama' => 'Kimia 11 & 12', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [3, 5]],

            ['id' => 23, 'nama' => 'Fisika 10', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [1]],
            ['id' => 24, 'nama' => 'Fisika 11 & 12', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [3, 5]],

            ['id' => 25, 'nama' => 'Geografi 10', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [2]],
            ['id' => 26, 'nama' => 'Geografi 11 & 12', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [4, 6]],

            ['id' => 27, 'nama' => 'Sejarah IPS 10', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [2]],
            ['id' => 28, 'nama' => 'Sejarah IPS 11 & 12', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [4, 6]],

            ['id' => 29, 'nama' => 'Sosiologi 10', 'jam' => '3',  'total_jam_pelajaran' => 3, 'pertemuan' => 1, 'classes' => [2]],
            ['id' => 30, 'nama' => 'Sosiologi 11 & 12', 'jam' => '4',  'total_jam_pelajaran' => 4, 'pertemuan' => 1, 'classes' => [4, 6]],
        ];

        foreach ($data as $data) {
            $subject = new Subject();
            $subject->nama = $data['nama'];
            $subject->jam = $data['jam'];
            $subject->total_jam_pelajaran = $data['total_jam_pelajaran'];
            $subject->pertemuan = $data['pertemuan'];
            $subject->save();
            $subject->classes()->sync($data['classes']);
        }
    }
}
