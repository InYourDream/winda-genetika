<?php

use Illuminate\Database\Seeder;
use App\Models\Major;

class MajorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $majors = [
            ['nama' => 'MIPA', 'alias' => 'Matematika dan Ilmu Pengetahuan'],
            ['nama' => 'IPS', 'alias' => 'Ilmu Pengetahuan Sosial'],
        ];

        foreach ($majors as $major) {
            Major::create($major);
        }
    }
}
