<?php

use Illuminate\Database\Seeder;
use App\Models\Grade;
use App\Models\Teacher;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teachers = [
            ['id' => '2', 'nama' => 'Suharjo, S.Pd'],
            ['id' => '3', 'nama' => 'Triyono, S.Pd'],
            ['id' => '6', 'nama' => 'Sumaryanti, S.Pd, Ek'],
            ['id' => '7 ', 'nama' => 'Bernadeta S, S.Ag'],
            ['id' => '8', 'nama' => 'Antonius Tukimin, S.Pd'],
            ['id' => '9', 'nama' => 'Yuyuniarti, S.Pd'],
            ['id' => '10', 'nama' => 'Hesti Pamuji R, S.Pd'],
            ['id' => '11', 'nama' => 'Mimi Susanti, S.Pd'],
            ['id' => '12', 'nama' => 'Suratmi, S.Pd'],
            ['id' => '13', 'nama' => 'Cucu Dariah, S.Pd'],
            ['id' => '14', 'nama' => 'Kristologus Bai, S.Ag'],
            ['id' => '15', 'nama' => 'Edwin H. Simanjuntak, SP'],
            ['id' => '16', 'nama' => 'Mastiah Hasanah, S.Pd'],
            ['id' => '17', 'nama' => 'Ria Asti Nugroho, SS'],
            ['id' => '18', 'nama' => 'Darmawansyah, S.H.I'],
            ['id' => '19', 'nama' => 'Yanti Indrawati, SP'],
            ['id' => '20', 'nama' => 'Norita Diana, S.Hut'],
            ['id' => '21', 'nama' => 'Fitriana Octaviani, S.Pd'],
            ['id' => '22', 'nama' => 'Nuryanti, S.Si'],
            ['id' => '23', 'nama' => 'Rohani Yulianti,S.Pd'],
            ['id' => '24', 'nama' => 'Dewi Agustiyarni, S.Pd'],
            ['id' => '25', 'nama' => 'Suryati, S.Th'],
            ['id' => '26', 'nama' => 'Nyemas Sari Wulan Aprilia,S.Pd.'],
            ['id' => '27', 'nama' => 'Dina Kurniawati, S.Pd'],
            ['id' => '28', 'nama' => 'Elisabet, S.Sos'],
            ['id' => '29', 'nama' => 'Djarobi, BA'],
            ['id' => '30', 'nama' => 'Welan'],
            ['id' => '31', 'nama' => 'Firdaus, S.Pd'],
            ['id' => '32', 'nama' => 'Z. Asmarady, S.Pd'],
            ['id' => '33', 'nama' => 'Anang Purwanto, S.Pd'],
            ['id' => '34', 'nama' => 'Wiwi Pitria, S.Pd'],
            ['id' => '35', 'nama' => 'Johan Kurniawan Budi, S.Sn'],
            ['id' => '36', 'nama' => 'Viktorianus Toding, S.Pd'],
            ['id' => '37', 'nama' => 'Sudarmi, S.Pd'],
            ['id' => '38', 'nama' => 'Marina Kurniasih S, S.Pd'],
            ['id' => '39', 'nama' => 'Fransiskus Andut, S.Pd'],
            ['id' => '40', 'nama' => 'Trilis Hartati,S.Pd'],
            ['id' => '41', 'nama' => 'Rona Ramadhan, S.Pd'],
            ['id' => '42', 'nama' => 'Saripa Manurung, S.Pd'],
            ['id' => '43', 'nama' => 'Erli Marlida, S.Pd'],
            ['id' => '44', 'nama' => 'Kuswanda, S.Pd'],
            ['id' => '45', 'nama' => 'Nurhidayati, S.Pd'],
            ['id' => '46', 'nama' => 'Dea Aries Fitriani,S.Pd'],
            ['id' => '47', 'nama' => 'Sodikun, S.Sos'],
            ['id' => '48', 'nama' => 'Kristina'],
            ['id' => '51', 'nama' => 'Edwar Rusliman, S.Pd'],
            ['id' => '52', 'nama' => 'Melyana Dewi,S.Pd'],
            ['id' => '53', 'nama' => 'Rahmi Sapariyanti,S.Pd'],
            ['id' => '54', 'nama' => 'Hasmuniasyah,S.Pd'],
            ['id' => '55', 'nama' => 'Apolonius Suhardi,S.Ag'],
            ['id' => '56', 'nama' => 'Fatima Tuzahrah,S.Pd'],
            ['id' => '57', 'nama' => 'Rendy Suwarno, S.Pd'],
            ['id' => '58', 'nama' => 'Heni, S.Pd'],
        ];

        foreach ($teachers as $teacher) {
            Teacher::create($teacher);
        }
    }

    public function createTeacher($subjectId)
    {
        return factory(App\Models\Teacher::class)->create([
            'subject_id' => $subjectId,
        ]);
    }

    public function old()
    {
        $subjects = App\Models\Subject::with('classes')->get();
        foreach ($subjects as $subject) {
            $id = $subject->id;
            $teacherX = $this->createTeacher($id);
            $teacherXI = $this->createTeacher($id);
            $teacherXII = $this->createTeacher($id);

            $idClassLevels = $subject->classes->pluck('id')->toArray();
            if ($idClassLevels == [1, 2, 3, 4, 5, 6]) {
                $teacherX->classes()->sync([1, 2]);
                $teacherXI->classes()->sync([3, 4]);
                $teacherXII->classes()->sync([5, 6]);
            } elseif ($idClassLevels == [1, 3, 5]) {
                $teacherX->classes()->sync([1]);
                $teacherXI->classes()->sync([3]);
                $teacherXII->classes()->sync([5]);
            } else {
                $teacherX->classes()->sync([2]);
                $teacherXI->classes()->sync([4]);
                $teacherXII->classes()->sync([6]);
            }
        }
    }
}
