<?php

use Illuminate\Database\Seeder;
use App\Models\Day;
use App\Models\Time;

class TimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $times = [
            0 => ['jam_mulai' => '07:00', 'jam_selesai' => '07:45'],
            1 => ['jam_mulai' => '07:45', 'jam_selesai' => '08:30'],
            2 => ['jam_mulai' => '08:30', 'jam_selesai' => '09:15'],
            //istrirahat 09:15 - 09:35
            3 => ['jam_mulai' => '09:30', 'jam_selesai' => '10:15'],
            4 => ['jam_mulai' => '10:15', 'jam_selesai' => '11:00'],
            5 => ['jam_mulai' => '11:00', 'jam_selesai' => '11:45'],
            //istrirahat 11:45 - 12:15
            6 => ['jam_mulai' => '12:15', 'jam_selesai' => '13:00'],
            7 => ['jam_mulai' => '13:00', 'jam_selesai' => '13:45'],
            8 => ['jam_mulai' => '13:45', 'jam_selesai' => '14:30'],
            9 => ['jam_mulai' => '14:30', 'jam_selesai' => '15:15'],
        ];

        $days = Day::all();

        foreach ($days as $day) {
            foreach ($times as $index => $time) {
                $time['day_id'] = $day->id;
                //senin ada upacara
                if ($time['day_id'] == 1 & $time['jam_mulai'] == '07:00') continue;

                //jumat lebih awal pulang
                if ($day->id == 5 & in_array($index, [6,7,8,9])) continue;

                Time::create($time);
            }
        }
    }
}
