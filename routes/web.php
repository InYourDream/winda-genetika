<?php

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/simple', 'SimpleController@simpleGeneticAlgorithms');

    Route::get('/', 'MainController@home')->name('home');
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
    Route::get('/ubah-password', 'MainController@changePassword')->name('ubah.password');
    Route::put('/ubah-password', 'MainController@updatePassword')->name('update.password');

    Route::resource('/waktu', 'TimeController');
    Route::resource('/kelas', 'GradeController');
    Route::resource('/pelajaran', 'SubjectController');
    Route::resource('/guru', 'TeacherController');

    //Tugas
    Route::get('/tugas', 'TaskController@index')->name('tugas.index');
    Route::get('/tugas/create/{id?}', 'TaskController@create')->name('tugas.create');
    Route::post('/tugas', 'TaskController@store')->name('tugas.store');
    Route::get('/tugas/{teacher_id}/{subject_id}', 'TaskController@edit')->name('tugas.edit');
    Route::put('/tugas/{teacher_id}/{subject_id}', 'TaskController@update')->name('tugas.update');
    Route::delete('/tugas/{teacher_id}/{subject_id}', 'TaskController@destroy')->name('tugas.destroy');

    Route::get('/jadwal/preview', 'ScheduleController@preview')->name('jadwal.preview');
    Route::get('/jadwal/cetak', 'ScheduleController@cetak')->name('jadwal.cetak');
    Route::resource('/jadwal', 'ScheduleController');

    Route::get('/generate', 'GenerateController@index')->name('generate.index');
    Route::get('/generate-jadwal', 'GenerateController@generate')->name('generate.jadwal');
    Route::post('/generate-jadwal', 'GenerateController@store')->name('generate.jadwal.store');

    Route::get('/ajax/tugas-guru/{id}', 'AjaxController@TaskTeacher');
    Route::get('/ajax/hari/{id}', 'AjaxController@day');
    Route::get('/ajax/kelas/{id}', 'AjaxController@grade');
    Route::get('/ajax/mata-pelajaran/{id}', 'AjaxController@subjectByClass');
    Route::get('/ajax/guru/{classLevelId}/{subjectId}/', 'AjaxController@teacher');

    Route::get('/home', 'MainController@home')->name('home');
});
