$('.time').mask('00:00');
$('.select2').select2();
$('[data-toggle="tooltip"]').tooltip();
$('.btn-delete').click(function (e) {
    e.preventDefault()
    swal({
        title: "Apakah yakin ingin di hapus ?",
        icon: "warning",
        buttons: ['Tidak', 'Ya'],
        dangerMode: true,
    }).then(function (konfirmasi) {
        if (konfirmasi) $(e.target).closest('form').submit()
    });
});
