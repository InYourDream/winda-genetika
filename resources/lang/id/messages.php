<?php

return [
    'simpan' => 'Data Berhasil di simpan !!',
    'update' => 'Data Berhasil di update !!',
    'hapus' => 'Data Berhasil di hapus !!',

    'waktu tidak valid' => "Jam mulai dan akhir yang di-input-kan tidak valid (:jam_mulai - :jam_selesai).",
    'waktu kebentrok' => "Mohon maaf jam tersebut kebentrok dengan jam :jam_mulai - :jam_selesai.",
    'jadwal sudah ada' => "Jadwal sudah ada mohon pilih hari dan jam yang lain.",
];
