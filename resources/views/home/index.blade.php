@extends('layouts.app')

@section('styles')
<style>
    .fa-question-circle {
        font-size: 12px;
        transform: translate(2px, -2px) ;
    }

    .value {
        font-size: 16px;
        font-weight: 600;
    }

    a {
        color: black;
    }

    a:hover {
        text-decoration: none;
    }
</style>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Home</h5>
            <div class="row">
                <div class="col-md-3">
                    <a href="{{ route('waktu.index') }}">
                    <div class="media border p-3">
                        <img src="{{ asset('assets/img/clock.png') }}" alt="jam" class="mr-3" style="width:60px;">
                            <div class="media-body mt-2">
                                <div class="title">
                                    <span class="h6 text-uppercase">Total jam</span>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" title="Dalam Seminggu"></i>
                                </div>
                                <div class="value">{{ $total['time'] }}</div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="{{ route('kelas.index') }}">
                        <div class="media border p-3">
                            <img src="{{ asset('assets/img/blackboard.png') }}" alt="kelas" class="mr-3" style="width:60px;">
                            <div class="media-body mt-2">
                                <div class="title">
                                    <span class="h6 text-uppercase">Total Kelas</span>
                                </div>
                                <div class="value">{{ $total['class'] }}</div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="{{ route('pelajaran.index') }}">
                        <div class="media border p-3">
                            <img src="{{ asset('assets/img/library.png') }}" alt="pelajaran" class="mr-3" style="width:60px;">
                            <div class="media-body mt-2">
                                <div class="title">
                                    <span class="h6 text-uppercase">Total pelajaran</span>
                                </div>
                                <div class="value">{{ $total['subject'] }}</div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="{{ route('guru.index') }}">
                        <div class="media border p-3">
                            <img src="{{ asset('assets/img/teacher.png') }}" alt="guru" class="mr-3" style="width:60px;">
                            <div class="media-body mt-2">
                                <div class="title">
                                    <span class="h6 text-uppercase">Total Guru</span>
                                </div>
                                <div class="value">{{ $total['teacher'] }}</div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-md-3">
                    <a href="{{ route('generate.index') }}">
                        <div class="media border p-3">
                            <img src="{{ asset('assets/img/gen.png') }}" alt="gen" class="mr-3" style="width:60px;">
                            <div class="media-body mt-2">
                                <div class="title">
                                    <span class="h6 text-uppercase">Generate Jadwal</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-3">
                    <a href="{{ route('jadwal.index') }}">
                        <div class="media border p-3">
                            <img src="{{ asset('assets/img/calendar.png') }}" alt="gen" class="mr-3" style="width:60px;">
                            <div class="media-body mt-2">
                                <div class="title">
                                    <span class="h6 text-uppercase">Total Jadwal</span>
                                </div>
                                <div class="value">{{ $total['schedule'] }}</div>
                            </div>
                        </div>
                    </a>
                </div>

                {{-- <div class="col-md-3">
                    <a href="{{ route('generate.index') }}">
                        <div class="media border p-3">
                            <img src="{{ asset('assets/img/gear.png') }}" alt="gen" class="mr-3" style="width:60px;">
                            <div class="media-body mt-2">
                                <div class="title">
                                    <span class="h6 text-uppercase">Pengaturan</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div> --}}
            </div>
        </div>
    </div>
@endsection
