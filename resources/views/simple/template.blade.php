<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Algoritma Genetika</title>
</head>
<body>
    <h1>Target : {{ $target }}</h1>
    <h1>solution : <span id="solution"></span></h1>
    <h1>Generation : <span id="generation">0</span></h1>
    @for ($i = 0; $i < $LengthPopulation; $i++)
        <div class="logging">
            <h2>chromosome - {{ $i + 1 }} : <span id="solution-{{ $i }}"></span> | Fitness : <span id="fitness-{{ $i }}"></span>%</h2>
        </div>
    @endfor
</body>
</html>

<script>
    var population = null

    function logging(population, generation, solution) {
        document.getElementById('solution').innerHTML = solution;
        document.getElementById('generation').innerHTML = generation;
        for (const index in population) {
            document.getElementById("solution-" + index).innerHTML = population[index].chromosome
            document.getElementById("fitness-" + index).innerHTML = population[index].fitness
        }
    }
</script>
