@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>{{ $errors->first() }}
    </div>
@endif

@if (session()->has('pesan'))
  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>{{ session('pesan') }}</strong>
  </div>
@endif

