@extends('layouts.app')

@section('content')
<div class="card container">
    <div class="card-body">
        <h5 class="card-title">Waktu</h5>
        @include('includes.pesan')
        <form class="form-inline mb-3" action="{{ route('waktu.index') }}">
            <input type="text" name="jam_mulai" class="form-control mb-2 mr-2" placeholder="Jam mulai" value="{{ request()->jam_mulai }}">
            <input type="text" name="jam_akhir" class="form-control mb-2 mr-2" placeholder="Jam akhir" value="{{ request()->jam_akhir }}">
            <select name="day_id" class="form-control mb-2 mr-2">
                <option value="">Semua</option>
                @foreach ($days as $day)
                <option @if(request()->day_id == $day->id) selected @endif
                    value="{{ $day->id }}">{{ $day->nama }}</option>
                @endforeach
            </select>
            <button type="submit" class="btn btn-primary mb-2 mr-1" type="button"><i class="fas fa-search"></i></button>
            <a href="{{ route('waktu.create') }}" class="btn btn-success mb-2 mr-1"><i class="fas fa-plus"></i></a>
            <a href="{{ route('waktu.index') }}" class="btn btn-info mb-2 mr-1"><i class="fas fa-sync-alt"></i></a>
            <a href="{{ route('home') }}" class="btn btn-secondary mb-2 mr-1"><i class="fas fa-arrow-left"></i></a>
        </form>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Hari</th>
                    <th>Jam</th>
                    <th class="text-right pr-4">aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($times as $time)
                <tr @if($time->id == session()->get('updated_id')) class="table-success" @endif>
                    <td>{{ ($times->currentpage() - 1) * $times->perPage() + $loop->iteration }}.</td>
                    <td>{{ $time->day->nama }}</td>
                    <td>{{ $time->range }}</td>
                    <td class="text-right">
                        <a href="{{ route('waktu.edit', $time->id) }}">
                            <i class="fas fa-edit text-info mr-2"></i>
                        </a>
                        <form method="POST" class="d-inline-block" action="{{ route('waktu.destroy', $time->id) }}">
                            @csrf
                            @method('DELETE')
                            <a href="#" class="btn-delete">
                                <i class="fas fa-times text-danger mr-2"></i>
                            </a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="d-flex justify-content-center">
            {{ $times->appends($_GET)->links() }}
        </div>
    </div>
</div>
@endsection
