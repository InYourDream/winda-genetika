@extends('layouts.app')

@section('content')
<div class="card container">
    <div class="card-body">
        <h5 class="card-title">Edit Waktu</h5>
        @include('includes.pesan')
        <form action="{{ route('waktu.update', $time->id) }}" method="post">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Hari</label>
                        <select name="day_id" class="form-control">
                            @foreach ($days as $day)
                                <option
                                    @if(old('day_id', $time->day->id) == $day->id) selected @endif
                                    value="{{ $day->id }}">{{ $day->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Jam Mulai</label>
                        <input type="text" name="jam_mulai" class="form-control time" value="{{ old('jam_mulai', $time->jam_mulai) }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Jam Selesai</label>
                        <input type="text" name="jam_selesai" class="form-control time" value="{{ old('jam_selesai', $time->jam_selesai) }}">
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-info mr-2">Update</button>
            <a href="{{ session('url_waktu') }}">
                <button type="button" class="btn btn-secondary mr-2">Kembali</button>
            </a>
        </form>
    </div>
</div>
@endsection
