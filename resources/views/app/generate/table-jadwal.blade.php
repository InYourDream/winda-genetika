<div class="row" style="font-size: 13px">
@foreach ($solution['schedules'] as $className => $schedulesByDay)
    <div class="col-md-3">
        <p class="card-text">{{ $className }}</p>
        <table class="table table-sm mb-5">
            <tr>
                <th>Jam Ke</th>
                <th>Jam Mulai</th>
                <th>Nama Pelajaran</th>
                <th>ID Pengajar</th>
            </tr>
            @php
                $index = 1;
            @endphp
            @foreach ($schedulesByDay as $schedules)
                @foreach ($schedules as $schedule)
                <tr @if($schedule['bentrok'] > 0) class="table-danger" @endif
                    @if($schedule['penumpukan'] > 0) class="table-warning" @endif>
                    <td>{{ $index++ }}</td>
                    <td>{{ $schedule['jam_mulai'] }}</td>
                    <td class="text-nowrap">{{ $schedule['nama'] ?? null }}</td>
                    <td>{{ $schedule['teacher_id'] ?? null }}</td>
                </tr>
                @endforeach
            @endforeach
        </table>
    </div>

    {{-- <div class="col-md-3">
        <p class="card-text">{{ $className }}</p>
        <table class="table table-sm mb-5">
            <tr>
                <th>Nama MatPel</th>
                <th>Penumpukan</th>
            </tr>
            @foreach ($solution['subjects'][$className] as $subject)
            <tr>
                <td>{{ $subject['nama'] }}</td>
                <td>{{ $subject['penumpukan'] }}</td>
            </tr>
            @endforeach
        </table>
    </div> --}}
    @endforeach
</div>
