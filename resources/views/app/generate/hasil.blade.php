@extends('layouts.app')

@section('styles')
<style>
    #nilai_fitness::after {
        content: ' %';
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                @include('includes.pesan')
                <h5 class="card-title">Generate Jadwal</h5>
                <form class="form mb-3" action="{{ route('generate.jadwal') }}" method="GET">
                    <div class="form-group">
                        <label for="">Maksimal Generate</label>
                        <input type="number" name="max_generation" class="form-control mb-2 mr-2" value="{{ request()->max_generation ?? 10 }}">
                    </div>
                    <div class="form-group">
                        <label for="">Total Populasi</label>
                        <input type="number" name="population_length" class="form-control mb-2 mr-2" min="1" max="25"
                            value="{{ request()->total_population ?? 5 }}">
                        <small class="form-text text-muted">Nilai antara 5 ~ 25</small>
                    </div>
                    <div class="form-group">
                        <label for="">Crossover Rate</label>
                        <input type="number" name="crossover_rate" class="form-control mb-2 mr-2" min="0" max="100"
                            value="{{ request()->crossover_rate ?? 50 }}">
                        <small class="form-text text-muted">Nilai antara 0 ~ 100</small>
                    </div>
                    <div class="form-group">
                        <label for="">Mutation Rate</label>
                        <input type="number" name="mutation_rate" class="form-control mb-2 mr-2" min="0" max="100"
                            value="{{ request()->mutation_rate ?? 30 }}">
                        <small class="form-text text-muted">Nilai antara 0 ~ 100</small>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary mb-2 mr-1" type="button">Generate</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Hasil Generate</h5>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Info</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Generasi Ke</td>
                            <td id="generation">{{ $generation }}</td>
                        </tr>
                        <tr>
                            <td>Nilai Fitness</td>
                            <td id="nilai_fitness">{{ $solution['fitness']->nilai_fitness }}</td>
                        </tr>
                        <tr>
                            <td>Nilai Error</td>
                            <td id="nilai_errors">{{ $solution['fitness']->nilai_errors }}</td>
                        </tr>
                        <tr>
                            <td>Jumlah Bentrok</td>
                            <td id="total_bentrok">{{ $solution['fitness']->total_bentrok }}</td>
                        </tr>
                        <tr>
                            <td>Penumpukan</td>
                            <td id="total_duplikasi">{{ $solution['fitness']->nilai_duplikasi_pada_hari_sama }}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td><button type="submit" class="btn btn-primary" onclick="$('#schedule').submit()">Simpan Jadwal Baru {{ $request }}</button></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row mt-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Jadwal Generate</h5>
                <div id="table-jadwal">
                    @include('app.generate.table-jadwal')
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Form --}}
<form id="schedule" action="{{ route('generate.jadwal.store') }}" class="d-none" method="post">
    @csrf
    @foreach ($solution['schedules'] as $className => $schedulesByDay)
        @foreach ($schedulesByDay as $schedules)
            @foreach ($schedules as $schedule)
                <input type="text" name="time_id[]" value="{{ $schedule['id'] }}">
                <input type="text" name="subject_id[]" value="{{ $schedule['pelajaran_id'] }}">
                <input type="text" name="teacher_id[]" value="{{ $schedule['guru_id'] }}">
                <input type="text" name="grade_id[]" value="{{ $schedule['kelas_id'] }}">
            @endforeach
        @endforeach
    @endforeach
</form>
@endsection
