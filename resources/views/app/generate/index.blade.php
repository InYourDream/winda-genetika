@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                @include('includes.pesan')
                <h5 class="card-title">Generate Jadwal</h5>
                <form class="form mb-3" action="{{ route('generate.jadwal') }}" method="GET">
                    <div class="form-group">
                        <label for="">Maksimal Generate</label>
                        <input type="number" name="max_generation" class="form-control mb-2 mr-2" value="{{ request()->max_generation ?? 1000 }}">
                    </div>
                    <div class="form-group">
                        <label for="">Total Populasi</label>
                        <input type="number" name="population_length" class="form-control mb-2 mr-2" min="5" max="25"
                            value="{{ request()->population_length ?? 5 }}">
                        <small class="form-text text-muted">Nilai antara 5 ~ 25</small>
                    </div>
                    <div class="form-group">
                        <label for="">Crossover Rate</label>
                        <input type="number" name="crossover_rate" class="form-control mb-2 mr-2" min="0" max="100"
                            value="{{ request()->crossover_rate ?? 25 }}">
                        <small class="form-text text-muted">Nilai antara 0 ~ 100</small>
                    </div>
                    <div class="form-group">
                        <label for="">Mutation Rate</label>
                        <input type="number" name="mutation_rate" class="form-control mb-2 mr-2" min="0" max="100"
                            value="{{ request()->mutation_rate ?? 5 }}">
                        <small class="form-text text-muted">Nilai antara 0 ~ 100</small>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary mb-2 mr-1" type="button">Generate</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
