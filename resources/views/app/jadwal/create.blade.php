@extends('layouts.app')

@section('content')
<div class="card container">
    <div class="card-body">
        <h5 class="card-title">Tambah Jadwal</h5>
        @include('includes.pesan')
        <form action="{{ route('jadwal.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Hari</label>
                        <select name="day_id" id="day_id" class="form-control mb-2 mr-2">
                            @foreach ($days as $day)
                            <option @if(old('day_id') == $day->id) selected @endif
                                value="{{ $day->id }}">{{ $day->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Jam</label>
                        <select name="time_id" id="time_id" class="form-control select2 mb-2 mr-2"></select>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="">Tingkat Kelas</label>
                    @component('components.forms.selectTingkatKelas', [
                        'class' => 'mb-2 mr-2',
                        'classLevels' => $classLevels,
                        'null' => true,
                        'value' => request()->tingkat_kelas
                    ])
                    @endcomponent
                </div>
                <div class="col-md-3">
                    <label for="">Nama Kelas</label>
                    <select name="grade_id" id="grade_id" class="form-control select2 mb-2 mr-2"></select>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Mata Pelajaran</label>
                        <select name="subject_id" class="form-control" id="subject_id"></select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Guru</label>
                        <input type="text" name="nama_guru" id="nama_guru" class="form-control" value="{{ old('nama_guru') }}" readonly>
                        <input type="hidden" name="teacher_id" id="teacher_id" class="form-control" value="{{ request('teacher_id') }}">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success mr-2">Simpan</button>
            <a href="{{ session('url_jadwal') }}">
                <button type="button" class="btn btn-secondary mr-2">Kembali</button>
            </a>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    //initial awal
    var day_id = $("#day_id").val()
    var time_id =  @json(old('time_id'));
    var tingkat_kelas = @json(old('tingkat_kelas'));
    var grade_id = @json(old('grade_id'));
    var subject_id = @json(old('subject_id'));
    $('#tingkat_kelas').val(tingkat_kelas)
    getJam(day_id)
    getKelas(tingkat_kelas)
    getMataPelajaran(tingkat_kelas)

    $("#day_id").change(function () {
        var day_id = $(this).val()
        getJam(day_id)
    })

    function getJam(day_id) {
        if (day_id) {
            $.get(`${URL}/ajax/hari/${day_id}`, function (data) {
                $("#time_id").select2().empty()
                $("#time_id").select2({data: data})
                $("#time_id").select2("val", time_id);
            })
        } else {
            $("#time_id").select2().empty();
        }
    }

    $("#tingkat_kelas").change(function () {
        var class_level_id = $(this).val()
        getKelas(class_level_id)
        getMataPelajaran(class_level_id)
    })

    function getKelas(tingkat_kelas) {
        if (tingkat_kelas) {
            $.get(`${URL}/ajax/kelas/${tingkat_kelas}`, function (data) {
                $("#grade_id").select2().empty()
                $("#grade_id").select2({data: data})
                $('#grade_id').val(grade_id).trigger('change.select2');
            })
        } else {
            $("#grade_id").select2().empty();
        }
    }

    function getMataPelajaran(tingkat_kelas) {
        if (tingkat_kelas) {
            $.get(`${URL}/ajax/mata-pelajaran/${tingkat_kelas}`, function (data) {
                $("#subject_id").select2().empty()
                $("#subject_id").select2({data: data})
                $("#subject_id").select2("val", subject_id)

                var class_level_id = $("#tingkat_kelas").val()
                var subject_id = $("#subject_id").select2('val')
                getGuru(subject_id, class_level_id)
            })
        } else {
            $("#subject_id").select2().empty();
            $("#nama_guru").val(null)
            $("#teacher_id").val(null)
        }
    }

    $("#subject_id").change(function () {
        var subject_id = $(this).val()
        var class_level_id = $("#tingkat_kelas").val()
        getGuru(subject_id, class_level_id)
    })

    function getGuru(subject_id, class_level_id) {
        if (subject_id && class_level_id) {
            $.get(`${URL}/ajax/guru/${subject_id}/${class_level_id}`, function (data) {
                $("#nama_guru").val(data.nama)
                $("#teacher_id").val(data.id)
            })
        } else {
            $("#nama_guru").val(null)
            $("#teacher_id").val(null)
        }
    }
</script>
@endsection
