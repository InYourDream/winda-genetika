@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title"></h5>
        @foreach ($schedulesByClass as $nama_kelas => $schedules)
        <table class="table small table-bordered">
            <thead>
                <tr>
                    @php $count = count($schedules) + 3 @endphp
                    <th class="text-center" colspan="{{ $count }}">Kelas {{ $nama_kelas }}</th>
                </tr>
            </thead>
            <tr>
                <th>Hari</th>
                <th>Jam ke</th>
                <th>Waktu</th>
                @foreach ($schedules as $index => $schedule)
                    <th>{{ $index }}</th>
                @endforeach
            </tr>

            @php $before = null @endphp
            @foreach ($times as $index => $time)
                @php
                    if ($time->day_id != $before) $jam_ke = 1;
                    $before = $time->day_id;
                @endphp
                <tr>
                    @if ($jam_ke == 1)
                        <td>{{ $time->hari }}</td>
                    @else
                        <td></td>
                    @endif
                    <td>{{ $jam_ke++ }}</td>
                    <td>{{ $time->jam_mulai }} - {{ $time->jam_selesai }}</td>
                    @foreach ($schedules as $schedule)
                        @if (isset($schedule[$index]->subject->nama))
                            <td
                            @if(count($schedule[$index]->bentrok) > 0) class="table-danger" @endif
                            @if($schedule[$index]->overload > 0) class="table-warning" @endif
                            >{{ $schedule[$index]->subject->nama }}</td>
                        @else
                            <td> - </td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
        </table>
        @endforeach
    </div>
</div>
@endsection
