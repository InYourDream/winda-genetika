@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Mata Pelajaran</h5>
        @include('includes.pesan')
        <form class="form-inline mb-3" action="{{ route('jadwal.index') }}">
            @component('components.forms.selectTingkatKelas', [
                    'class' => 'mb-2 mr-2',
                    'classLevels' => $classLevels,
                    'null' => true,
                    'value' => request()->tingkat_kelas
                ])
            @endcomponent
            <input type="text" name="nama_kelas" class="form-control mb-2 mr-2" placeholder="Nama Kelas" value="{{ request()->nama_kelas }}">
            <select name="day_id" class="form-control mb-2 mr-2">
                <option value="">Semua</option>
                @foreach ($days as $day)
                <option @if(request()->day_id == $day->id) selected @endif
                    value="{{ $day->id }}">{{ $day->nama }}</option>
                @endforeach
            </select>
            <input type="text" name="jam_mulai" class="form-control mb-2 mr-2" placeholder="Jam mulai" value="{{ request()->jam_mulai }}">
            <input type="text" name="jam_akhir" class="form-control mb-2 mr-2" placeholder="Jam akhir" value="{{ request()->jam_akhir }}">
            <input type="text" name="subject" class="form-control mb-2 mr-2" placeholder="Mata Pelajaran" value="{{ request()->subject }}">
            <input type="text" name="teacher" class="form-control mb-2 mr-2" placeholder="Guru" value="{{ request()->teacher }}">
            <button type="submit" class="btn btn-primary mb-2 mr-1" type="button"><i class="fas fa-search"></i></button>
            <a href="{{ route('jadwal.create') }}" class="btn btn-success mb-2 mr-1"><i class="fas fa-plus"></i></a>
            <a href="{{ route('jadwal.index') }}" class="btn btn-info mb-2 mr-1"><i class="fas fa-sync-alt"></i></a>
            <a href="{{ route('home') }}" class="btn btn-secondary mb-2 mr-1"><i class="fas fa-arrow-left"></i></a>
        </form>

        <table class="table small table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kelas</th>
                    <th>Hari</th>
                    <th>Jam</th>
                    <th>Mata Pelajaran</th>
                    <th>Guru</th>
                    <th class="text-right pr-4">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($schedules as $schedule)
                <tr
                    @if($schedule->id == session()->get('updated_id')) class="table-success" @endif
                    @if(count($schedule->bentrok) > 0) class="table-danger" @endif
                    @if($schedule->overload > 0) class="table-warning" @endif
                    >
                    <td>{{ ($schedules->currentpage() - 1) * $schedules->perPage() + $loop->iteration }}.</td>
                    <td>{{ $schedule->grade->nama }}</td>
                    <td>{{ $schedule->time->day->nama }}</td>
                    <td>{{ $schedule->time->jam_mulai }} - {{ $schedule->time->jam_selesai }}</td>
                    <td>{{ $schedule->subject->nama ?? ' - ' }}</td>
                    <td>{{ $schedule->teacher->nama ?? ' - ' }}</td>
                    <td class="text-right">
                        <a href="{{ route('jadwal.edit', $schedule->id) }}">
                            <i class="fas fa-edit text-info mr-2"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
            </tfoot>
        </table>

        <div class="d-flex justify-content-center">
            {{ $schedules->appends($_GET)->links() }}
        </div>
    </div>
</div>
@endsection
