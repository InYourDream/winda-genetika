@extends('layouts.app')

@section('styles')
<style>
    p {
        margin-bottom: 0px;
    }

    hr {
        border-top: 3px solid black;
        border-style: double;
    }

    @media print {
        @page {
            margin: 20px;
        }

        body {
            margin: 0px 0px 0px 0px;
        }

        table {
            font-size: 12px !important;
        }

        .break {
            page-break-after: always;
        }
    }

</style>
@endsection

@section('scripts')
<script>
    function cetak() {
        var Window = window.open("", "", "width=4000,height=4000");
        var head = document.head.innerHTML;
        var printcontent = document.getElementById("print").innerHTML;
        Window.document.body.innerHTML = head + printcontent;
        setTimeout(() => {
            Window.focus();
            Window.print();
            Window.close();
        }, 1500);
    }

</script>
@endsection

@section('content')
<div class="card">
    <div class="card-body" style="font-size: 13px;">
        <h5 class="card-title">
            <button class="btn btn-primary" onclick="cetak()">
                Cetak Jadwal
            </button>
        </h5>

        @foreach ($schedulesByClass as $schedulesByGrade)
        <div id="print">
            <div class="row mb-2">
                <div class="col-md-2">
                    <div style="position: absolute; top: 0; right: -50px">
                        <img class="float-right" height="110" src="{{ asset('assets/img/lambang-osis.jpg') }}" alt="">
                    </div>
                </div>
                <div class="col-md-8 h5">
                    <div class="text-center">
                        <p>PEMERINTAH PROVINSI KALIMANTAN BARAT</p>
                        <p>DINAS PENDIDIKAN DAN KEBUDAYAAN</p>
                        <p class="font-weight-bold">SMA NEGERI 1 NANGA PINOH</p>
                        <p style="font-size: 14px;">Jalan Kota Baru km. 1 Nanga Pinoh,Kab.Melawi, Pos:79672</p>
                        <p></p>
                        <p class="font-weight-bold">Akreditasi: A</p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div style="position: absolute; left:-50px">
                        <img height="110" src="{{ asset('assets/img/lambang-sekolah.jpg') }}" alt="">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row mb-2">
                <div class="col-md-2"></div>
                <div class="col-md-8 text-center h6 font-weight-bold">
                    <p>JADWAL PELAJARAN SEMESTER GENAP</p>
                    <p>TAHUN PELAJARAN {{ date("Y") - 1 }} / {{ date("Y") }}</p>
                </div>
                <div class="col-md-2"></div>
            </div>
            <table class="table small table-bordered table-sm d-print-block mb-2">
                <thead class="text-center">
                    <th class="align-middle" style="width: 1%">Hari</th>
                    <th class="align-middle" style="width: 90px;">Waktu</th>
                    <th class="align-middle" style="width: 1%">Jam ke</th>
                    @foreach ($schedulesByGrade as $nama_kelas => $schedules)
                    <th class="align-middle">{{ $nama_kelas }}</th>
                    @endforeach
                </thead>
                @php
                $pointer = -1;
                @endphp
                @foreach($days as $indexDay => $day)
                @php
                $count = count($day->times) + 1;
                if ($day->nama == 'Senin') {
                $count += 1;
                }
                @endphp
                <tr>
                    <td rowspan="{{ $count }}" class="h4 align-middle text-center text-capitalize">
                        @for ($i = 0; $i < strlen($day->nama); $i++)
                            <div>{{ $day->nama[$i] }}</div>
                            @endfor
                    </td>
                </tr>
                @foreach ($day->times as $indexTime => $time)
                @if($day->nama == 'Senin' && $indexTime == 0)
                <tr class="text-center">
                    <td>07.00 - 07.45</td>
                    <td>{{ $indexTime + 1 }}</td>
                    <td colspan="{{ count($schedulesByGrade) }}">UPACARA</td>
                </tr>
                @endif

                <tr class="text-center">
                    <td>{{ $time['jam_mulai'] }} - {{ $time['jam_selesai'] }}</td>
                    @if($day->nama == 'Senin')
                    <td>{{ $indexTime + 2 }}</td>
                    @else
                    <td>{{ $indexTime + 1 }}</td>
                    @endif
                    @php
                    $pointer += 1;
                    @endphp
                    @foreach ($schedulesByGrade as $nama_kelas => $schedules)
                    <td>{{ $schedules[$pointer]->subject->nama ?? '' }}</td>
                    @endforeach
                </tr>
                @endforeach
                @endforeach
            </table>
            <div class="row mt-3 mb-5 break">
                <div class="col-md-6">
                    <div class="float-left" style="font-size: 13px; margin-left: 150px;">
                        <p>Mengetahui Pengawas Pembina,</p>
                        <div style="margin-bottom: 95px"></div>
                        <p><strong><u>H. NURCHOLIS, S.Pd</u></strong></p>
                        <p>Pembina</p>
                        <p>NIP. 19620615 198601 1 004</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="float-right" style="font-size: 13px; margin-right: 150px;">
                        <p>Nannga Pinoh, <span class="text-uppercase">{{ Carbon\Carbon::now()->addMonths(1)->formatLocalized('%d %B %Y') }}</span></p>
                        <p>PLT. Kepala Sekolah</p>
                        <div style="margin-bottom: 75px"></div>
                        <p><strong><u>ANTONIUS TUKIMIN,S.Pd</u></strong></p>
                        <p>PLT. Kepala Sekolah</p>
                        <p>Pembina</p>
                        <p>NIP.198010112005021003</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
