@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Mata Pelajaran</h5>
        @include('includes.pesan')
        <form class="form-inline mb-3" action="{{ route('pelajaran.index') }}">
            <input type="text" name="nama" class="form-control mb-2 mr-2" placeholder="Mata Pelajaran" value="{{ request()->nama }}">
            @component('components.forms.selectKelas', [
                    'class' => 'mb-2 mr-2',
                    'null' => true,
                    'value' => request()->kelas
                ])
            @endcomponent
            @component('components.forms.selectJurusan', [
                    'class' => 'mb-2 mr-2',
                    'null' => true,
                    'value' => request()->major_id
                ])
            @endcomponent
            <button type="submit" class="btn btn-primary mb-2 mr-1" type="button"><i class="fas fa-search"></i></button>
            <a href="{{ route('pelajaran.create') }}" class="btn btn-success mb-2 mr-1"><i class="fas fa-plus"></i></a>
            <a href="{{ route('pelajaran.index') }}" class="btn btn-info mb-2 mr-1"><i class="fas fa-sync-alt"></i></a>
            <a href="{{ route('home') }}" class="btn btn-secondary mb-2 mr-1"><i class="fas fa-arrow-left"></i></a>
        </form>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Mata Pelajaran</th>
                    <th class="text-center">Total Jam</th>
                    @foreach ($classLevels as $class)
                        <th class="text-center">{{ $class->nama }}</th>
                    @endforeach
                    <th class="text-right pr-4">aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($subjects as $subject)
                <tr @if($subject->id == session()->get('updated_id')) class="table-success" @endif>
                    <td>{{ ($subjects->currentpage() - 1) * $subjects->perPage() + $loop->iteration }}.</td>
                    <td>{{ $subject->nama }}</td>
                    <td class="text-center">{{ $subject->total_jam_pelajaran }}</td>
                    @foreach ($classLevels as $key => $class)
                        <td class="text-center">
                            @if (in_array($class->id, $subject->classes->pluck('id')->toArray()))
                                <i class="fas fa-check text-success "></i>
                            @endif
                        </td>
                    @endforeach
                    <td class="text-right">
                        <a href="{{ route('pelajaran.edit', $subject->id) }}">
                            <i class="fas fa-edit text-info mr-2"></i>
                        </a>
                        <form method="POST" class="d-inline-block" action="{{ route('pelajaran.destroy', $subject->id) }}">
                            @csrf
                            @method('DELETE')
                            <a href="#" class="btn-delete">
                                <i class="fas fa-times text-danger mr-2"></i>
                            </a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th class="text-center" colspan="2">TOTAL</th>
                    <th class="text-center">{{ $total->total_jam_pelajaran}}</th>
                    <th class="text-center">{{ $total->total_pertemuan }}</th>
                    <th colspan="{{ 2 + $classLevels->count() }}"></th>
                </tr>
            </tfoot>
        </table>

        <div class="d-flex justify-content-center">
            {{ $subjects->appends($_GET)->links() }}
        </div>
    </div>
</div>
@endsection
