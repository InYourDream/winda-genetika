@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Tambah Pelajaran</h5>
        @include('includes.pesan')
        <form action="{{ route('pelajaran.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Nama Mata Pelajaran</label>
                        <input type="text" name="nama" class="form-control" value="{{ old('nama') }}">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Total Jam Pelajaran</label>
                        <input type="number" name="jam" class="form-control" value="{{ old('jam_pelajaran') }}" min="1" max="4">
                        <div class="form-text text-muted">Direkomendasikan input 1 - 4 jam</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label id="labelKelas" for="">Kelas</label>
                        @component('components.forms.checkKelas', [
                        'subject' => session('store') ?? null,
                        'classes' => $classes
                        ])
                        @endcomponent
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success mr-2">Simpan</button>
            <a href="{{ session('url_pelajaran') }}">
                <button type="button" class="btn btn-secondary mr-2">Kembali</button>
            </a>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $("#semuaKelas").click(function () {
        $('input[name="classes[]"]').prop('checked', this.checked);
    });
</script>
@endsection
