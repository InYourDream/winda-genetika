@extends('layouts.app')

@section('content')
<div class="card container">
    <div class="card-body">
        <h5 class="card-title">Kelas</h5>
        @include('includes.pesan')
        <form class="form-inline mb-3" action="{{ route('kelas.index') }}">
            <input type="text" name="nama" class="form-control mb-2 mr-2" placeholder="Kelas" value="{{ request()->nama }}">
            @component('components.forms.selectKelas', [
                    'class' => 'mb-2 mr-2',
                    'null' => true,
                    'value' => request()->kelas
                ])
            @endcomponent
            @component('components.forms.selectJurusan', [
                    'class' => 'mb-2 mr-2',
                    'null' => true,
                    'value' => request()->major_id
                ])
            @endcomponent
            <button type="submit" class="btn btn-primary mb-2 mr-1" type="button"><i class="fas fa-search"></i></button>
            <a href="{{ route('kelas.create') }}" class="btn btn-success mb-2 mr-1"><i class="fas fa-plus"></i></a>
            <a href="{{ route('kelas.index') }}" class="btn btn-info mb-2 mr-1"><i class="fas fa-sync-alt"></i></a>
            <a href="{{ route('home') }}" class="btn btn-secondary mb-2 mr-1"><i class="fas fa-arrow-left"></i></a>
        </form>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kelas / Jurusan</th>
                    <th>Kelas</th>
                    <th>Jurusan</th>
                    <th class="text-right pr-4">aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($grades as $grade)
                <tr @if($grade->id == session()->get('updated_id')) class="table-success" @endif>
                    <td>{{ ($grades->currentpage() - 1) * $grades->perPage() + $loop->iteration }}.</td>
                    <td>{{ $grade->nama }}</td>
                    <td>{{ $grade->kelas }}</td>
                    <td>{{ $grade->jurusan }}</td>
                    <td class="text-right">
                        <a href="{{ route('kelas.edit', $grade->id) }}">
                            <i class="fas fa-edit text-info mr-2"></i>
                        </a>
                        <form method="POST" class="d-inline-block" action="{{ route('kelas.destroy', $grade->id) }}">
                            @csrf
                            @method('DELETE')
                            <a href="#" class="btn-delete">
                                <i class="fas fa-times text-danger mr-2"></i>
                            </a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="d-flex justify-content-center">
            {{ $grades->appends($_GET)->links() }}
        </div>
    </div>
</div>
@endsection
