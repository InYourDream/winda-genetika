@extends('layouts.app')

@section('content')
<div class="card container">
    <div class="card-body">
        <h5 class="card-title">Tambah Kelas</h5>
        @include('includes.pesan')
        <form action="{{ route('kelas.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Kelas</label>
                        @component('components.forms.selectKelas', [
                                'value' => old('kelas', session('store.kelas'))
                            ])
                        @endcomponent
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Jurusan</label>
                        @component('components.forms.selectJurusan', [
                                'value' => old('major_id', session('store.major_id'))
                            ])
                        @endcomponent
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Nama Kelas</label>
                        <input type="text" name="nama" class="form-control" value="{{ old('nama') }}">
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success mr-2">Simpan</button>
            <a href="{{ session('url_kelas') }}">
                <button type="button" class="btn btn-secondary mr-2">Kembali</button>
            </a>
        </form>
    </div>
</div>
@endsection
