@extends('layouts.app')

@section('content')
<div class="card container">
    <div class="card-body">
        <h5 class="card-title">Guru</h5>
        @include('includes.pesan')
        <form class="form-inline mb-3" action="{{ route('guru.index') }}">
            <input type="text" name="nama" class="form-control mb-2 mr-2" style="min-width:25%;" placeholder="Nama Guru" value="{{ request()->nama }}">
            <button type="submit" class="btn btn-primary mb-2 mr-1" type="button"><i class="fas fa-search"></i></button>
            <a href="{{ route('guru.create') }}" class="btn btn-success mb-2 mr-1"><i class="fas fa-plus"></i></a>
            <a href="{{ route('guru.index') }}" class="btn btn-info mb-2 mr-1"><i class="fas fa-sync-alt"></i></a>
            <a href="{{ route('home') }}" class="btn btn-secondary mb-2 mr-1"><i class="fas fa-arrow-left"></i></a>
        </form>

        <table id="datatable" class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th class="align-middle" rowspan="2">ID</th>
                    <th class="align-middle" rowspan="2">Nama Guru</th>
                    <th class="text-right pr-4">aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($teachers as $teacher)
                <tr @if($teacher->id == session()->get('updated_id')) class="table-success" @endif>
                    <td>{{ $teacher->id }}.</td>
                    <td>{{ $teacher->nama }}</td>
                    <td class="text-right">
                        <a href="{{ route('guru.edit', $teacher->id) }}">
                            <i class="fas fa-edit text-info mr-2"></i>
                        </a>
                        <form method="POST" class="d-inline-block" action="{{ route('guru.destroy', $teacher->id) }}">
                            @csrf
                            @method('DELETE')
                            <a href="#" class="btn-delete">
                                <i class="fas fa-times text-danger mr-2"></i>
                            </a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="d-flex justify-content-center">
            {{ $teachers->appends($_GET)->links() }}
        </div>
    </div>
</div>
@endsection

