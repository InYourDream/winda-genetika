@extends('layouts.app')

@section('content')
<div class="card container">
    <div class="card-body">
        <h5 class="card-title">Tambah Guru</h5>
        @include('includes.pesan')
        <form action="{{ route('guru.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">ID / Kode Guru</label>
                        <input type="text" name="id" class="form-control" value="{{ old('id') }}">
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="">Nama Guru</label>
                        <input type="text" name="nama" class="form-control" value="{{ old('nama') }}">
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success mr-2">Simpan</button>
            <a href="{{ session('url_guru') }}">
                <button type="button" class="btn btn-secondary mr-2">Kembali</button>
            </a>
        </form>
    </div>
</div>
@endsection
