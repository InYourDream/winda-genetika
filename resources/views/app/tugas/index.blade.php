@extends('layouts.app')

@section('styles')
    <style>
        .table > tbody > tr > td {
            background-color: white;
        }

        .DTFC_RightWrapper {
            right: 0px !important;
        }
    </style>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Tugas Mengajar Guru</h5>
        @include('includes.pesan')
        <form class="form-inline mb-3" action="{{ route('tugas.index') }}">
            <input type="text" name="nama" class="form-control mb-2 mr-2" style="min-width:25%;" placeholder="Nama Guru" value="{{ request()->nama }}">
            <input type="text" name="mata_pelajaran" class="form-control mb-2 mr-2" style="min-width:25%;" placeholder="Mata Pelajaran" value="{{ request()->mata_pelajaran }}">
            <a href="{{ route('tugas.create') }}" class="btn btn-success mb-2 mr-1"><i class="fas fa-plus"></i></a>
            <button type="submit" class="btn btn-primary mb-2 mr-1" type="button"><i class="fas fa-search"></i></button>
            <a href="{{ route('tugas.index') }}" class="btn btn-info mb-2 mr-1"><i class="fas fa-sync-alt"></i></a>
            <a href="{{ route('home') }}" class="btn btn-secondary mb-2 mr-1"><i class="fas fa-arrow-left"></i></a>
        </form>

        <table id="datatable" class="table table-hover table-bordered text-nowrap">
            <thead>
                <tr>
                    <th class="align-middle" rowspan="2">ID</th>
                    <th class="align-middle" rowspan="2">Nama</th>
                    <th class="align-middle" rowspan="2">Mata Pelajaran</th>
                    @foreach ($classLevels as $class)
                        <th class="text-center" colspan="{{ $class->count() }}">KELAS {{ $class[0]->nama }} </th>
                    @endforeach
                    <th class="align-middle text-center" rowspan="2">Total</th>
                    <th class="align-middle text-center" rowspan="2">aksi</th>
                </tr>
                <tr>
                    @foreach ($grades as $grade)
                        <th>{{ $grade->nama }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($teachers as $key => $teacher)
                @php
                    $subjects = $teachers[$key]->subjects->unique('nama');
                    $mapel = [];
                    $total_jam = 0;
                    foreach ($subjects as $subject) {
                        $mapel[] = ['id' => $subject->id, 'nama' => $subject->nama, 'jam' => $subject->total_jam_pelajaran];
                    }
                    $count = count($mapel);
                    $count = $count == 0 ? 1 : $count;
                    $total_jam = 0;
                    for ($i = 0; $i < $count; $i++) {
                        foreach ($grades as $key => $grade) {
                            $jam[$i][$key] = checkList($mapel, $i, $data, $teacher, $grade);
                            $total_jam += $jam[$i][$key];
                        }
                    }
                @endphp
                <tr>
                    <td rowspan="{{ $count }}">{{ $teacher->id }}.</td>
                    <td rowspan="{{ $count }}">
                        {{ $teacher->nama }}
                        <a class="text-right ml-2" href="{{ route('tugas.create', $teacher->id) }}"><i class="fas fa-xs fa-plus text-success"></i></a>
                    </td>
                    <td>{{ $mapel[0]['nama'] ?? '' }}</td>
                    @foreach ($grades as $key => $grade)
                        <td class="text-center">{{ $jam[0][$key] }}</td>
                    @endforeach
                    <td class="align-middle text-center" rowspan="{{ $count }}">{{ $total_jam }}</td>
                    <td class="text-left">
                        @if (isset($mapel[0]))
                            <a href="{{ route('tugas.edit', [$teacher->id, $mapel[0]['id']]) }}">
                                <i class="fas fa-edit text-info mr-2"></i>
                            </a>
                            <form method="POST" class="d-inline-block" action="{{ route('tugas.destroy', [$teacher->id, $mapel[0]['id']]) }}">
                                @csrf
                                @method('DELETE')
                                <a href="#" class="btn-delete">
                                    <i class="fas fa-times text-danger mr-2"></i>
                                </a>
                            </form>
                        @endif
                    </td>
                </tr>
                    @for ($i = 1; $i < $count; $i++)
                        <tr>
                            <td style="display: none;"></td>
                            <td style="display: none;"></td>
                            <td>{{ $mapel[$i]['nama'] }}</td>
                            @foreach ($grades as $key => $grade)
                                <td class="text-center">{{ $jam[$i][$key] }}</td>
                            @endforeach
                            <td style="display: none;"></td>
                            <td class="text-left">
                                <a href="{{ route('tugas.edit', [$teacher->id, $mapel[$i]['id']]) }}">
                                    <i class="fas fa-edit text-info mr-2"></i>
                                </a>
                                <form method="POST" class="d-inline-block" action="{{ route('tugas.destroy', [$teacher->id, $mapel[0]['id']]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <a href="#" class="btn-delete">
                                        <i class="fas fa-times text-danger mr-2"></i>
                                    </a>
                                </form>
                            </td>
                        </tr>
                    @endfor
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="3" class="text-right">TOTAL</th>
                    @foreach ($grades as $grade)
                        <th class="text-center">
                            @php
                            try {
                                echo $total[$grade->id];
                            } catch (\Throwable $th) {
                            }
                            @endphp
                        </th>
                    @endforeach
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>

        <div class="d-flex mt-3 justify-content-center">
            {{ $teachers->appends($_GET)->links() }}
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var table = $('#datatable').DataTable({
            paging: false,
            ordering: false,
            info: false,
            searching: false,
            scrollX: true,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 3,
                rightColumns: 2
            },
            sScrollXInner: "100%"
        })
    </script>
@endsection
