@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Tambah Tugas Guru</h5>
        @include('includes.pesan')
        <form action="{{ route('tugas.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Nama Guru</label>
                        <select name="teacher_id" class="form-control " id="teacher_id">
                            <option value=""></option>
                            @foreach ($teachers as $teacher)
                                <option
                                    @if(old('teacher_id', session('store.teacher_id')) == $teacher->id) selected @endif
                                    value="{{ $teacher->id }}">{{ $teacher->id }}. {{ $teacher->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Mata Pelajaran</label>
                        <select name="subject_id" class="form-control select2" id="mata_pelajaran">
                            <option value=""></option>
                            @foreach ($subjects as $subject)
                                <option
                                    @if(old('subject_id', session('store.subject_id')) == $subject->id) selected @endif
                                     value="{{ $subject->id }}">{{ $subject->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Kelas</label>
                        <div class="row">
                            @foreach ($classLevels as $level)
                                <div class="col-md-2" id="wrapper__{{ $level->id }}">
                                    <div class="form-check abc-checkbox abc-checkbox-success">
                                        <input class="form-check-input" id="all__{{ $level->id }}" type="checkbox">
                                        <label class="form-check-label" for="all__{{ $level->id }}">Semua {{ $level->nama }}</label>
                                    </div>
                                    @foreach ($level->grades as $grade)
                                    <div class="form-check abc-checkbox abc-checkbox-success">
                                        <input class="form-check-input all__{{ $level->id }}" id="class__{{ $grade->id }}" name="grade_id[]" type="checkbox" value="{{ $grade->id }}">
                                        <label class="form-check-label" for="class__{{ $grade->id }}">{{ $grade->nama }}</label>
                                    </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success mr-2">Simpan</button>
            <a href="{{ session('url_tugas') }}">
                <button type="button" class="btn btn-secondary mr-2">Kembali</button>
            </a>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    let id =  {{ $id ?? 'null' }};
    let levels = [1, 2, 3, 4, 5, 6];

    if (id) {
        $('#teacher_id').val(id).trigger("change");
        $('#teacher_id').prop("disabled", true);
        getTugas(id);
    }

    $('form').submit(function (e) {
        $('#teacher_id').prop("disabled", false);
    })

    levels.forEach(el => {
        $(`#all__${el}`).change(function() {
            let checked = this.checked;
            $(`.all__${el}`).each(function () {
                let check = $(this).prop("disabled")
                if (!check) $(this).prop('checked', checked);
            })
        })
    });

    $("#teacher_id").select2({
        placeholder: "- Pilih Guru -"
    });

    $("#teacher_id").change(function () {
        let id_guru = $(this).val()
        getTugas(id_guru)
    })

    function getTugas(id_guru) {
        resetCheckBox()
        $.get(`${URL}/ajax/tugas-guru/${id_guru}`, function (data) {
            data.forEach(el => {
                $(`#class__${el}`).prop('disabled', true)
            })
        })
    }

    function resetCheckBox() {
        $('.form-check-input').prop('disabled', false)
    }
</script>
@endsection
