<select name="major_id" id="major_id" class="form-control {{ $class ?? null }}">
    @if(isset($null))
        <option value="">Semua Jurusan</option>
    @endif
    <option @if($value == "1") selected @endif value="1">MIPA</option>
    <option @if($value == "2") selected @endif value="2">IPS</option>
</select>
