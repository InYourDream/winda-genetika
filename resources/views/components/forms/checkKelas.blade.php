<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="semuaKelas">
            <label class="custom-control-label" for="semuaKelas">Semua Kelas</label>
        </div>
    </div>
</div>

<div class="row">
    @foreach ($classes as $class)
    <div class="col-md-6 col-sm-12">
        <div class="custom-control custom-checkbox">
            <input name="classes[]" type="checkbox" class="custom-control-input" id="{{ $class->nama }}"
                value="{{ $class->id }}" {{ checkClass($subject, $class->id) }}>
            <label class="custom-control-label" for="{{ $class->nama }}">{{ $class->nama }}</label>
        </div>
    </div>
    @endforeach
</div>
