<br>
<div class="custom-control custom-checkbox custom-control-inline">
    <input name="majors[]" type="checkbox" class="custom-control-input" id="IPS" value="1" {{ checkMajor($subject, '1') }}>
    <label class="custom-control-label" for="IPS">IPS</label>
</div>
<div class="custom-control custom-checkbox custom-control-inline">
    <input name="majors[]" type="checkbox" class="custom-control-input" id="MIPA" value="2" {{ checkMajor($subject, '2') }}>
    <label class="custom-control-label" for="MIPA">MIPA</label>
</div>
