<select name="kelas" class="form-control {{ $class ?? null}}" id="kelas">
    @if(isset($null))
        <option value="">Semua Kelas</option>
    @endif
    <option @if($value == 'X') selected @endif>X</option>
    <option @if($value == 'XI') selected @endif>XI</option>
    <option @if($value == 'XII') selected @endif>XII</option>
</select>
