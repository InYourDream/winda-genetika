<select name="tingkat_kelas" id="tingkat_kelas" class="form-control {{ $class ?? null}}" id="kelas">
    @if(isset($null))
        <option value="" readonly></option>
    @endif
    @if(isset($semua))
        <option value="">Semua Kelas</option>
    @endif
    @foreach ($classLevels as $classLevel)
        <option @if($value == $classLevel->id) selected @endif value={{ $classLevel->id }}>{{ $classLevel->nama }}</option>
    @endforeach
</select>
