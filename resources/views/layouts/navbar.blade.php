<nav class="navbar navbar-expand-lg my-navbar navbar-dark bg-dark sticky-top">
    <a class="navbar-brand" href="{{ route('home') }}">Penjadwalan Mata Pelajaran</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar" aria-controls="myNavbar"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="myNavbar">
        @auth
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Master
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('waktu.index') }}">Waktu</a>
                    <a class="dropdown-item" href="{{ route('kelas.index') }}">Kelas</a>
                    <a class="dropdown-item" href="{{ route('pelajaran.index') }}">Mata Pelajaran</a>
                    <a class="dropdown-item" href="{{ route('guru.index') }}">Guru</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('tugas.index') }}">Tugas Mengajar Guru</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Jadwal
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('generate.index') }}">Generate</a>
                    {{-- <a class="dropdown-item" href="{{ route('jadwal.index') }}">Ubah Jadwal</a> --}}
                    <a class="dropdown-item" href="{{ route('jadwal.cetak') }}">Cetak Jadwal</a>
                    {{-- <a class="dropdown-item" href="{{ route('jadwal.preview') }}">Lihat Jadwal</a> --}}
                </div>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('ubah.password') }}"><i class="fas fa-key text-light"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}"><i class="fas fa-user-plus text-light"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt text-light"></i></a>
            </li>
            @endauth
        </ul>
    </div>
</nav>
