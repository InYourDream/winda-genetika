<body>
    {{-- jQuery first, then Popper.js, then Bootstrap JS  --}}
    <script src="{{ asset('vendor/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('vendor/popper.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.fixedColumns.min.js') }}"></script>

    <script src="{{ asset('vendor/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/sweetalert.min.js') }}"></script>
    {{-- Custom JS --}}
    <script src="{{ asset('js/initial.js') }}"></script>
    <script>var URL =  "{{ url('/') }}";</script>
    @yield('scripts')
</body>

