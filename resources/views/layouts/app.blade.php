<!DOCTYPE html>
<html lang="id">
    @include('layouts.header')
    <body>
        <div id="app">
            @include('layouts.navbar')
            <main class="main m-4">
                @yield('content')
            </main>
        </div>
    @include('layouts.scripts')
    </body>
</html>

