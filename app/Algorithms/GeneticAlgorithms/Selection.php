<?php

namespace App\Algorithms\GeneticAlgorithms;

class Selection {

    static public function roullete($population)
    {
        for ($i=1; $i <= 2; $i++) {
            [$total_fitness, $findKey] = self::totalFitness($population);
            $rand = mt_rand(0, 100);
            $fitness = [];
            foreach ($population as $key => $populate) {
                if ($key == 0) $sum = 0;
                $sum += round($populate['fitness']->nilai_fitness / $total_fitness * 100, 2);
                $fitness[$key] = $sum;
                if (max($rand, $sum) == $rand) continue;
                $findKey = $key;
                break;
            }

            //remove from population
            $parent[$i] = $population[$findKey];
            unset($population[$findKey], $fitness[$findKey]);
        }

        return $parent;
    }

    static function totalFitness($population)
    {
        $total_fitness = 0;
        foreach ($population as $populate) {
            $total_fitness += $populate['fitness']->nilai_fitness;
        }
        end($population);
        $endKey = key($population);
        return [$total_fitness, $endKey];
    }

    static function bestFitness($population)
    {
        foreach ($population as $key => $populate) {
            $fitness[$key] = $populate['fitness']->nilai_fitness;
        }

        $max = max($fitness);
        $key = array_search($max, $fitness);
        $best = $population[$key];
        unset($population[$key], $fitness[$key]);

        return $best;
    }
}
