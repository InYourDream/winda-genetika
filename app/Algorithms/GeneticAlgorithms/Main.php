<?php

namespace App\Algorithms\GeneticAlgorithms;
use App\Algorithms\GeneticAlgorithms\{Population, Selection, Mutation, Generation, Logging};
use App\Models\{Grade, Time, Subject};
use App\Models\Day;

class Main {

    //pengaturan nilai default
    public $populationLength = 5;
    public $crossoverRate = 50;
    public $mutationRate = 30;
    public $totalGen = 0;
    public $maxGeneration = 100;

    public $grades;
    public $times;
    public $maxTime;
    public $maxDay;
    public $days;
    public $subjectsByClass;
    public $teachersByClass;

    //ambil data dari database
    public function __construct()
    {
        // $grades = [1,2,3,4,5,6,7,8];
        // $grades = [9,10,11,12,13,14,15,16];
        // $grades = [17,18,19,20,21,22,23,24];
        $grades = null;
        $this->grades = Grade::joinClassLevels()->only($grades);
        $this->days = Day::with('times')->get();
        $this->totalDay = count($this->days);
        $this->totalTime = Time::count();
        $this->subjectByGrade = Subject::fetchByGradeId($grades);
        $this->listSubject = $this->listingSubject($this->subjectByGrade);
    }

    //Mata pelajaran sesuaikan dengan jam & pertemuan
    public function listingSubject($subjectByGrade)
    {
        $newSubject = [];
        foreach ($subjectByGrade as $indexClass => $subjects) {
            $group = 'A';
            $subjectIndex = -1;
            $slot = 0;
            $isGradeTen = ($indexClass[0] == "X" && $indexClass[1] == ' ') ? true : false;
            foreach ($subjects as $subject) {
                $total_jam_pelajaran = $subject->total_jam_pelajaran;
                $blocks = $isGradeTen == true ? $this->staticBlok($total_jam_pelajaran) : $this->randomBlok($total_jam_pelajaran);
                foreach ($blocks as $index => $block) {
                    $subjectIndex++;
                    for ($i=0; $i < $block; $i++) {
                        $status = ["duplicate" => true];
                        if ($i == 0) $status['duplicate'] = false;
                        $merge = [
                            "length" => $block,
                            "tingkat_kelas" => $indexClass,
                            "group" => $group,
                        ];
                        $build = array_merge($subject->toArray(), $status, $merge);
                        $newSubject[$indexClass][$subjectIndex][$i] = $build;
                        $slot++;
                    }
                    $group++;
                }
            }
        }
        return $newSubject;
    }

    public function randomBlok($total_jam_pelajaran)
    {
        if ($total_jam_pelajaran == 4) {
            if (mt_rand(0, 3) == 3) return [2, 2];
            return [1, 3];
        } else if ($total_jam_pelajaran == 3) {
            if (mt_rand(0, 5) == 5) return [3];
            return [1, 2];
        }
        return [$total_jam_pelajaran];
    }

    public function staticBlok($total_jam_pelajaran)
    {
        if ($total_jam_pelajaran == 1) return [1];
        if ($total_jam_pelajaran == 4) {
            return [2, 2];
        } else if ($total_jam_pelajaran == 3) {
            return [3];
        }
        return [$total_jam_pelajaran];
    }

    //algoritma utama di jalankan
    public function solution()
    {
        ini_set('max_execution_time', 99999);
        $time_start = microtime(true);
        $init = $this;
        $population = Population::generate($init);
        $generation = 0;
        $solution = Selection::bestFitness($population);
        Logging::first($solution, $generation);
        while ($generation < $this->maxGeneration) {
            $generation++;
            $parents = Selection::roullete($population);
            $children = CrossOver::nPoint($parents, $init, $this->crossoverRate);
            $mutants = (new Mutation)->swap($children, $init, $this->mutationRate, $generation);
            $population = Generation::commit($population, $mutants);
            $solution = Selection::bestFitness($population);
            Logging::update($solution, $generation);
            if ($solution['fitness']->nilai_fitness >= 100) break;
        }

        $time_end = microtime(true);
        $execution_time = gmdate("H:i:s", $time_end - $time_start);
        Logging::result($solution, $generation, $execution_time);
    }

    //validasi jika tidak ada kelas dan total jam pelajaran lebih banyak dari waktu tersedia
    public function validate()
    {
        if (count($this->grades) == 0) {
            $pesan = "Tidak ada kelas yang terdaftar !";
            return $pesan;
        }

        foreach ($this->subjectByGrade as $grade => $subjects) {
            $total_jam_pelajaran_dalam_seminggu = 0;
            foreach ($subjects as $subject) {
                $total_jam_pelajaran_dalam_seminggu += $subject->total_jam_pelajaran;
            }
            $isGradeTen = ($grade[0] == "X" && $grade[1] == ' ') ? true : false;
            if ($isGradeTen) $total_jam_pelajaran_dalam_seminggu += 2;
            if ($total_jam_pelajaran_dalam_seminggu < $this->totalTime) {
                $pesan = "
                    Kelas $grade mempunyai jumlah pelajaran $total_jam_pelajaran_dalam_seminggu
                    lebih sedikit dari daftar jumlah waktu yaitu {$this->totalTime}";
                return $pesan;
            }

            if ($total_jam_pelajaran_dalam_seminggu > $this->totalTime) {
                $pesan = "
                    Kelas $grade mempunyai jumlah pelajaran $total_jam_pelajaran_dalam_seminggu
                    lebih besar dari daftar jumlah waktu yaitu {$this->totalTime}";
                return $pesan;
            }
        }

        return null;
    }
}
