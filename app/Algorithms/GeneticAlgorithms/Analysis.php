<?php

namespace App\Algorithms\GeneticAlgorithms;

class Analysis {

    public $subjectsByClass;
    public $schedules;
    public $analyses;
    public $grades;
    public $days;
    public $maxTime;

    public function __construct($schedules, $init)
    {
        $this->schedules = $schedules;
        $this->subjectByGrade = $init->subjectByGrade;
        $this->days = $init->days;
        $this->grades = $init->grades;
    }

    public function evaluation()
    {
        $schedulesByClass = $this->schedules;
        foreach ($schedulesByClass as $kelas => $schedulesByDay) {
            foreach ($schedulesByDay as $hari => $schedules) {
            $this->maxTime = count($schedules);
                for ($time = 0; $time < $this->maxTime; $time++) {
                    $this->schedules[$kelas][$hari][$time] =
                    $this->analyze($schedules, $schedules[$time], $kelas, $hari, $time);
                }
            }
        }

        return $this->schedules;
    }

    public function analyze($schedules, $schedule, $kelas, $hari, $time)
    {
        $ujiJadwal = $this->schedules[$kelas][$hari][$time];
        $ujiJadwal['bentrok'] = 0;
        $ujiJadwal['penumpukan'] = -1;

        //cek jika kosong
        if (!isset($ujiJadwal['subject_id'])) {
            $ujiJadwal['penumpukan'] = 0;
            return $ujiJadwal;
        }

        if ($ujiJadwal['duplicate'] == true || !isset($ujiJadwal['subject_id'])) {
            $ujiJadwal['penumpukan'] = 0;
        } else {
            $idSubject = $ujiJadwal['subject_id'];
            foreach ($schedules as $schedule) {
                if ($schedule['duplicate'] == true || !isset($schedule['subject_id'])) continue;
                if ($idSubject == $schedule['subject_id']) $ujiJadwal['penumpukan'] += 1;
            }
        }

        //cek bentrok pada kelas lain
        $jadwal_pertama = $ujiJadwal;
        foreach ($this->grades as $gradeIndex => $grade) {
            $kelas = $grade->nama;
            if (!isset($this->schedules[$kelas][$hari][$time])) continue;
            $jadwal_kedua =  $this->schedules[$kelas][$hari][$time];
            $cekBentrok = $this->cekBentrok($jadwal_pertama, $jadwal_kedua);
            if ($cekBentrok == true) {
                $ujiJadwal['bentrok'] += 1;
            }
        }

        return $ujiJadwal;
    }

    public function cekBentrok($jadwal_pertama, $jadwal_kedua)
    {
        if ($jadwal_pertama['grade_id'] == $jadwal_kedua['grade_id']) return false;
        if ($jadwal_pertama['teacher_id'] == $jadwal_kedua['teacher_id']) return true;
        return false;
    }
}
