<?php

namespace App\Algorithms\GeneticAlgorithms;

class Chromosome {
    public $days;
    public $schedule = [];
    public $grade_id;
    public $subjects;
    public $gen;

    public function schedule()
    {
        $gen = new Gen;
        $gen->subjects = $this->subjects;
        $gen->grade_id = $this->grade_id;
        $isClassTen = $this->class_level_id == 1 || $this->class_level_id == 2;

        while (True) {
            foreach ($this->days as $indexHari => $day) {
                $gen->times = $day->times;
                $gen->timeIndex = 0;
                $totalTimes = count($gen->times);
                $this->gen = $gen;

                if ($isClassTen == true && $indexHari == 0) {
                    $totalTimes -= 2;
                }

                while ($gen->timeIndex < $totalTimes) {
                    // jika sisa tersisa 1 atau 2 jam
                    $sisa = $totalTimes - $gen->timeIndex;
                    if ($sisa <= 2 && $sisa > 0) {
                        $created = $gen->createMin($sisa);
                        //jika tidak ada pelajaran maka reset ulang
                        if (!$created) {
                            $gen->timeIndex = 0;
                            $gen->subjects = $this->subjects;
                            $this->schedule = [];
                            break;
                        }
                        $this->inputSubject($created, $indexHari, $gen);
                        continue;
                    }
                    // Pilih paket pelajaran & Masukin
                    if (count($gen->subjects) == 0) break;
                    $created = $gen->choose();
                    $this->inputSubject($created, $indexHari, $gen);
                }
            }
            // dd($this->schedule, $gen->subjects);
            if (count($gen->subjects) == 0) break;
        }

        //isi dengan jadwal kosong pada kelas 10 pada hari senin
        if ($isClassTen) {
            $gen->times = $this->days[0]->times;
            $gen->timeIndex = 6;
            $this->schedule[0][6] = $gen->createJamKosong();
            $gen->timeIndex = 7;
            $this->schedule[0][7] = $gen->createJamKosong();
        }


        $reschedule = [];
        for ($i=0; $i < count($this->schedule); $i++) {
            $reschedule[$i] = $this->schedule[$i];
        }

        return $reschedule;
    }

    public function createJamKosong($indexHari, $gen)
    {
        $this->schedule[$indexHari][$gen->timeIndex] = $gen->createJamKosong();
    }

    public function inputSubject($created, $indexHari, $gen)
    {
        foreach ($created as $create) {
            $this->schedule[$indexHari][$gen->timeIndex] = $gen->create($create);
        }
    }
}
