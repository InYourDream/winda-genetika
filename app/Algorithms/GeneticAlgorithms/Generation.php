<?php

namespace App\Algorithms\GeneticAlgorithms;

class Generation {

    public static function commit($population, $mutants)
    {

        $population =  array_merge($population, $mutants);
        foreach ($population as $key => $populate) {
            $fitness[$key] = $populate['fitness']->nilai_fitness;
        }

        //eliminasi 2 terkecil
        for ($i=1; $i <= 2; $i++) {
            $min = min($fitness);
            $key = array_search($min, $fitness);
            unset($population[$key], $fitness[$key]);
        }

        $generation = [];
        foreach ($population as $populate) {
            $generation[] = $populate;
        }

        return $generation;
    }
}
