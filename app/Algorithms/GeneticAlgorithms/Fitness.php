<?php

namespace App\Algorithms\GeneticAlgorithms;

class Fitness {

    public $nilai_fitness = 0;
    public $nilai_errors = 0;

    public $nilai_bentrok = 0;
    public $nilai_penumpukan = 0;

    public $total_bentrok = 0;
    public $total_penumpukan = 0;

    public function calculate($schedulesByClass)
    {
        foreach ($schedulesByClass as $kelas => $schedulesByDay) {
            foreach ($schedulesByDay as $hari => $schedules) {
                foreach ($schedules as $schedule) {
                    $this->total_bentrok += $schedule['bentrok'];
                    $this->total_penumpukan += $schedule['penumpukan'];
                }
            }
        }

        $this->nilai_bentrok = $this->total_bentrok;
        $this->nilai_penumpukan = $this->total_penumpukan;
        $this->nilai_errors = $this->nilai_bentrok + $this->nilai_penumpukan;
        $this->nilai_fitness = round(100 / (1 + $this->nilai_errors), 4);
    }
}
