<?php

namespace App\Algorithms\GeneticAlgorithms;
use Illuminate\Http\Request;
use App\Models\Subject;

class Gen {

    public $times;
    public $timeIndex;
    public $grade_id;
    // public $tingkat_kelas;
    public $subjects;
    public $single;
    public $group;

    public function waktu() {
        $waktu = $this->times[$this->timeIndex];
        return [
            'id_waktu' => $waktu->id,
            'jam_mulai' => $waktu->jam_mulai,
            'jam_selesai' => $waktu->jam_selesai,
        ];
    }

    public function createJamKosong()
    {
        $jamKosong = $this->waktu() +
            Subject::createJamKosong()->toArray() +
            ['group' => null] +
            ['duplicate' => false] +
            ['grade_id' => $this->grade_id] +
            // ['tingkat_kelas_id' => $this->class_level_id] +
            ['mutable' => false];
        $this->timeIndex++;

        return $jamKosong;
    }

    public function choose()
    {
        $key = $this->random = array_rand($this->subjects);
        $subject = $this->subjects[$this->random];
        unset($this->subjects[$key]);
        return $subject;
    }

    public function create($subject)
    {
        $jadwal = $this->waktu() + $subject;
        $this->timeIndex++;
        return $jadwal;
    }

    public function createMin($min)
    {
        $listMin = [];
        //filter subject yang kurang dari minimal
        foreach ($this->subjects as $index => $subject) {
            $total = count($subject);
            if ($total <= $min ) $listMin[$index] = $total;
        }
        if (!$listMin) return null;
        $key = $this->random = array_rand($listMin);
        $subject = $this->subjects[$this->random];
        if (count($subject) == 1) $this->single += 1;
        unset($this->subjects[$key]);
        return $subject;
    }
}
