<?php

namespace App\Algorithms\GeneticAlgorithms;

class Individual {

    public $gradeIndex = 0;
    public $timeIndex = 0;
    public $totalTimes = 0;

    public $times = [];
    public $schedules = [];
    public $subjects = [];

    public function create($init)
    {
        $grades = $init->grades;
        $listSubject = $init->listSubject;
        $days = $init->days;

        foreach ($grades as $grade) {
            $nama_kelas = $grade->nama;
            $choromosome = new Chromosome;
            $choromosome->days = $days;
            $choromosome->subjects = $listSubject[$grade->nama];
            $choromosome->grade_id = $grade->id;
            $choromosome->class_level_id = $grade->class_level_id;
            $this->schedules[$nama_kelas] = $choromosome->schedule();
        }

        return $this->schedules;
    }
}
