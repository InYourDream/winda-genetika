<?php

namespace App\Algorithms\GeneticAlgorithms;

class CrossOver {

    static public function nPoint($parents, $init, $crossOverRate)
    {
        $children = $parents;

        foreach ($parents[1]["schedules"] as $indexGrade => $grades) {
            $rand = mt_rand(0, 100);
            if ($rand <= $crossOverRate) {
                $children[1]['schedules'][$indexGrade] = $parents[2]['schedules'][$indexGrade];
                $children[2]['schedules'][$indexGrade] = $parents[1]['schedules'][$indexGrade];
            }
        }

        return $children;
    }

    static public function twoPoint($parents, $length, $crossOverRate)
    {
        $crossOver = $crossOverRate / 100 * $length;
        $start_point = mt_rand(0, $length - $crossOver);
        $start_point = 0;
        $end_point = $start_point + $crossOver - 1;
        $current_point = -1;
        $children = $parents;

        foreach ($parents[1]["schedules"] as $indexGrade => $grades) {
            foreach ($grades as $indexTime => $schedule) {
                $current_point++;
                if ($current_point < $start_point) continue;
                $children[1]['schedules'][$indexGrade][$indexTime] = $parents[2]['schedules'][$indexGrade][$indexTime];
                $children[2]['schedules'][$indexGrade][$indexTime] = $parents[1]['schedules'][$indexGrade][$indexTime];
            }
        }

        return $children;
    }

}
