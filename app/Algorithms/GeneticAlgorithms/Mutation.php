<?php

namespace App\Algorithms\GeneticAlgorithms;

use Illuminate\Support\Str;


class Mutation {

    public function swap($children, $init, $mutationRate, $generation)
    {
        $mutants = $children;
        // $force_mutant = 1;
        if ($generation >= 0) $force_mutant = mt_rand(0, 1);

        foreach ($children as $childIndex => $child) {
            foreach ($child['schedules'] as $kelas => $schedulesByGrade) {
                foreach ($schedulesByGrade as $indexDay => $schedulesByTimes) {
                    foreach ($schedulesByTimes as $indexTime => $schedule) {
                        // jika tidak bisa di ubah lewatkan
                        if (isset($schedule['mutable'])) continue;
                        // jika lebih rendah maka tidak mutasi
                        $rand = mt_rand(0, 100);
                        if (($schedule['bentrok'] == 0 && $schedule['penumpukan'] == 0) || $rand > $mutationRate) continue;

                        // force mutasi
                        if ($force_mutant == 1) {
                            $maxDay = count($schedulesByTimes);
                            $group = $schedulesByGrade[$indexDay][$indexTime]['group'];
                            $schedulesByGrade[$indexDay][$indexTime]['duplicate'] = false;
                            $schedulesByGrade[$indexDay][$indexTime]['length'] = 1;
                            $schedulesByGrade[$indexDay][$indexTime]['group'] = Str::random(10);
                            //mutasi length
                            $up = $down = false;
                            $history = [];
                            for ($i=0; $i < $maxDay; $i++) {
                                if ($group == $schedulesByTimes[$i]['group']) {
                                    if ($indexTime > $i) {
                                        $up = true;
                                        $history[] = $i;
                                        $schedulesByGrade[$indexDay][$i]['length'] -= 1;
                                    }
                                    if ($indexTime < $i) {
                                        $down = true;
                                        $history[] = $i;
                                        $schedulesByGrade[$indexDay][$i]['length'] -= 1;
                                    }
                                }
                            }

                            if ($up && $down) {
                                foreach ($history as $h) {
                                    $schedulesByGrade[$indexDay][$h]['length'] = -1;
                                }
                            }

                            $schedule = $schedulesByGrade[$indexDay][$indexTime];
                        };

                        // proses mutasi
                        if ($schedule['duplicate'] == true) continue;
                        $total_length = 0;
                        while ($schedule['length'] != $total_length) {
                            $sel_day = mt_rand(0, 4);
                            $total_waktu = count($schedulesByGrade[$sel_day]);
                            $sel_time = mt_rand(0, $total_waktu - 1);
                            $select_schedule = $schedulesByGrade[$sel_day][$sel_time];
                            if ($select_schedule['duplicate'] == true) continue;
                            if ($select_schedule['teacher_id'] == null) continue;
                            $total_length = $select_schedule['length'];
                            if ($schedule['length'] == $total_length) break;
                        }

                        // Tukar jadwal
                        for ($i=0; $i < $total_length; $i++) {
                            $temp = $schedulesByGrade[$indexDay][$indexTime + $i];
                            $change = $schedulesByGrade[$sel_day][$sel_time + $i];
                            $dataTemp = [
                                'id_waktu' => $temp['id_waktu'],
                                'jam_mulai' => $temp['jam_mulai'],
                                'jam_selesai' => $temp['jam_selesai'],
                            ];
                            $dataChange = [
                                'id_waktu' => $change['id_waktu'],
                                'jam_mulai' => $change['jam_mulai'],
                                'jam_selesai' => $change['jam_selesai'],
                            ];
                            $schedulesByGrade[$indexDay][$indexTime + $i] = array_merge($change, $dataTemp);
                            $schedulesByGrade[$sel_day][$sel_time + $i] = array_merge($temp, $dataChange);
                        }
                    }
                }
                $mutants[$childIndex]['schedules'][$kelas] = $schedulesByGrade;
            }
        }


        foreach ($mutants as $key => $mutant) {
            //hitung fitness
            $analysis = new Analysis($mutant['schedules'], $init);
            $analysisSchedules = $analysis->evaluation();

            $fitness = (new Fitness);
            $fitness->calculate($analysisSchedules);

            $mutants[$key] = ['schedules' => $analysisSchedules, 'fitness' => $fitness];
        }

        return $mutants;
    }
}
