<?php

namespace App\Algorithms\GeneticAlgorithms;

use Illuminate\Support\Facades\App;

class Logging
{
    public static function first($solution, $generation)
    {
        echo view('app.generate.hasil-sementara', compact('solution', 'generation'));
    }

    public static function update($solution, $generation)
    {
        $fitness = ($solution['fitness']);
        echo view('app.generate.update', compact('fitness', 'generation'));
        if (App::environment('local')) {
            ob_flush();
        }
        flush();
    }

    public static function result($solution, $generation, $execution_time)
    {
        echo view('app.generate.scriptDoc', compact('solution', 'generation', 'execution_time'));
        if (App::environment('local')) {
            ob_flush();
        }
        flush();
    }
}
