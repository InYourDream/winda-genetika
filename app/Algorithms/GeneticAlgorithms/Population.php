<?php

namespace App\Algorithms\GeneticAlgorithms;

use App\Algorithms\GeneticAlgorithms\{Individual, Fitness};

class Population {

    //bangkitkan populasi
    public static function generate($init)
    {
        $populationLength = $init->populationLength;

        for ($i=0; $i < $populationLength; $i++) {
            $schedules = (new Individual)->create($init);

            $analysis = new Analysis ($schedules, $init);
            $analysisSchedules = $analysis->evaluation();

            $fitness = new Fitness;
            $fitness->calculate($analysisSchedules);

            $population[] = ['schedules' => $analysisSchedules, 'fitness' => $fitness];
        }

        return $population;
    }

}
