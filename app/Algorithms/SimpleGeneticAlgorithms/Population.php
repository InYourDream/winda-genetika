<?php

namespace App\Algorithms\SimpleGeneticAlgorithms;

use App\Algorithms\SimpleGeneticAlgorithms\{Chromosome, Fitness};

class Population {

    public static function generate($fitness, $lengthTarget, $LengthPopulation)
    {
        $population = [];
        for ($i=0; $i < $LengthPopulation; $i++) {
            $chromosome = (new Chromosome)->create($lengthTarget);
            $valueFitness = $fitness->calculate($chromosome);
            $population[] = ['chromosome' => $chromosome, 'fitness' => $valueFitness];
        }
        return $population;
    }
}
