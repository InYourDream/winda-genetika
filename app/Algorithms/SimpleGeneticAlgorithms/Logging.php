<?php

namespace App\Algorithms\SimpleGeneticAlgorithms;

class Logging {

    static public function template($LengthPopulation, $target)
    {
        echo view('simple/template', compact('LengthPopulation', 'target'));
    }

    static public function print($data, $delay)
    {
        echo view('simple/scriptLogging', $data);
        usleep($delay);
        flush();
        ob_flush();
    }
}
