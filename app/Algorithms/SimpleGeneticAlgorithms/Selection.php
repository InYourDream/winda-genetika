<?php

namespace App\Algorithms\SimpleGeneticAlgorithms;

class Selection {

    static public function bestFitness($population)
    {
        foreach ($population as $key => $populate) {
            $fitness[$key] = $populate['fitness'];
        }

        for ($i=1; $i <= 2; $i++) {
            $max = max($fitness);
            $key = array_search($max, $fitness);
            $parent[$i] = $population[$key];
            unset($population[$key], $fitness[$key]);
        }

        return $parent;
    }

}
