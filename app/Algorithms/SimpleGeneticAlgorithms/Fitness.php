<?php

namespace App\Algorithms\SimpleGeneticAlgorithms;

class Fitness {

    public $target = '';

    public function calculate($chromosome)
    {
        $gens = str_split($chromosome);
        $target = $this->target;
        $length = strlen($target);
        $value = 0;
        for ($i=0; $i < $length ; $i++) {
            if ($gens[$i] === $target[$i]) $value++;
        }
        $percent = round(($value / strlen($target)) * 100, 2);
        return $percent;
    }

}
