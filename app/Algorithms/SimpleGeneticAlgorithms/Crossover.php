<?php

namespace App\Algorithms\SimpleGeneticAlgorithms;

class CrossOver {

    static public function commit($parent)
    {
        $child = $parent;
        $CP = round(strlen($parent[1]['chromosome']) / 2);

        $replace = (substr($parent[2]['chromosome'], 0, $CP));
        $child[1] = substr_replace($child[1]['chromosome'], $replace, 0, $CP);

        $replace = (substr($parent[1]['chromosome'], 0, $CP));
        $child[2] = substr_replace($child[2]['chromosome'], $replace, 0, $CP);

        return $child;
    }

}
