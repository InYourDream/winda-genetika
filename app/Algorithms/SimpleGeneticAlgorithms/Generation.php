<?php

namespace App\Algorithms\SimpleGeneticAlgorithms;

class Generation {

    public static function commit($population, $mutants)
    {
        foreach ($population as $key => $populate) {
            $fitness[$key] = $populate['fitness'];
        }

        for ($i=1; $i <= 2; $i++) {
            $min = min($fitness);
            $removeKeys[] = $key = array_search($min, $fitness);
            unset($population[$key], $fitness[$key]);
        }

        return array_merge($population, $mutants);
        // return $population;
    }
}
