<?php

namespace App\Algorithms\SimpleGeneticAlgorithms;

class Mutation {

    public $rateMutation;

    public function __construct($rateMutation)
    {
        $this->rateMutation = $rateMutation;
    }

    public function commit($children, $fitness)
    {
        foreach ($children as $key => $child) {
            $length = strlen($child);
            for ($i=0; $i < $length; $i++) {
                $random = mt_rand(0, 10) / 10;
                if ($random <= $this->rateMutation) {
                    $child[$i] = chr(rand(32, 126));
                }
            }
            $mutant[$key]['chromosome'] = $child;
            $mutant[$key]['fitness'] = $fitness->calculate($child);
        }
        return $mutant;
    }

}
