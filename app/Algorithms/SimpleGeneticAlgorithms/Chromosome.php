<?php

namespace App\Algorithms\SimpleGeneticAlgorithms;

class Chromosome {

    public function create($length)
    {
        return str_random($length);
    }

}
