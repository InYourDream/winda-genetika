<?php

namespace App\Algorithms\SimpleGeneticAlgorithms;
use App\Algorithms\SimpleGeneticAlgorithms\{Population, Selection, Mutation, Generation, Logging};

class Main {

    public $target = 'laravel5';
    public $delay = 0;
    public $LengthPopulation = 5;
    public $mutationRate = 0.1;

    public function solution()
    {
        $target = $this->target;
        $LengthPopulation = $this->LengthPopulation;
        $lengthTarget = strlen($target);

        $fitness = new Fitness;
        $fitness->target = $this->target;

        $population = Population::generate($fitness, $lengthTarget, $LengthPopulation);
        $generation = 0;

        Logging::template($LengthPopulation, $target);

        while (true) {
            $generation++;

            $parents = Selection::bestFitness($population);
            $children = CrossOver::commit($parents);

            $mutation = new Mutation($this->mutationRate);
            $mutants = $mutation->commit($children, $fitness);

            $population = Generation::commit($population, $mutants);

            $solution = Selection::bestFitness($population);

            $log = [
                'population' => json_encode($population),
                'generation' => $generation,
                'solution' => $solution[1]['chromosome'],
            ];
            Logging::print($log, $this->delay);

            $fitness_100 = max(array_column($solution, 'fitness'));
            if ($fitness_100 == 100) break;
        }
    }
}
