<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassSubject extends Model
{
    function subjects()
    {
        return $this->hasMany('App\Models\Subject');
    }
}
