<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $fillable = ['nama', 'kelas', 'major_id'];

    public function class()
    {
        return $this->belongsTo('App\Models\ClassLevel', 'class_level_id');
    }

    public function getKelasAttribute()
    {
        return $this->class->kelas;
    }

    public function getJurusanAttribute()
    {
        return $this->class->major->nama ;
    }

    public static function fetch($request, $paginate = 10)
    {
        return self::where(function ($query) use ($request) {
                if ($request->nama) $query->where('nama', 'like', "%$request->nama%");
            })
            ->whereHas('class', function($query) use ($request) {
                if ($request->kelas) $query->where('kelas', "$request->kelas");
                if ($request->major_id) $query->where('major_id', "$request->major_id");
            })
            ->with('class', 'class.major')
            ->orderBy('class_level_id')
            ->orderBy('nama')
            ->paginate($paginate);
    }

    public static function joinClassLevels()
    {
        return self::join('class_levels', 'class_levels.id', '=', 'grades.class_level_id')
            ->select('grades.nama', 'grades.class_level_id', 'grades.id', 'class_levels.nama as level')
            ->orderBy('grades.class_level_id')
            ->orderBy('grades.nama')
            ->get();
    }

    public static function byClassLevelId($id)
    {
        $classLevels = ClassLevel::all();
        $class = [];
        foreach ($classLevels as $key => $level) {
            $class[] = self::where('class_level_id', $level->id)->get();
        }
        return $class;
    }
}
