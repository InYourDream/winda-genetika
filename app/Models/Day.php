<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    public function times()
    {
        return $this->hasMany('App\Models\Time');
    }
}
