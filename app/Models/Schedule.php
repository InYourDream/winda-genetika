<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    //
    public function time()
    {
        return $this->belongsTo('App\Models\Time');
    }

    public function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

    public function grade()
    {
        return $this->belongsTo('App\Models\Grade');
    }

    public static function fetch($request, $paginate = 10)
    {
        return $schedules = self::with('time', 'time.day', 'teacher', 'subject', 'grade')
            ->whereHas('time', function($query) use ($request) {
                $query->orderBy('jam_mulai');
                if ($request->jam_mulai) $query->where('jam_mulai', 'like', "%$request->jam_mulai%");
                if ($request->jam_selesai) $query->where('jam_selesai', 'like', "%$request->jam_selesai%");
                if ($request->day_id) $query->where('day_id', $request->day_id);
            })
            ->whereHas('grade', function($query) use ($request) {
                $query->orderBy('nama');
                if ($request->nama_kelas) $query->where('nama', 'like', "%$request->nama_kelas%");
                if ($request->tingkat_kelas) $query->where('class_level_id', "$request->tingkat_kelas");
            })
            ->where(function ($query) use ($request) {
                if ($request->subject) {
                    $query->whereHas('subject', function ($q) use ($request) {
                        $q->where('nama', 'like', "%$request->subject%");
                    });
                }

                if ($request->teacher) {
                    $query->whereHas('teacher', function($q) use ($request) {
                        if ($request->teacher) $q->where('nama', 'like', "%$request->teacher%");
                    });
                }

            })
            ->paginate($paginate);

    }

    public function getBentrokAttribute()
    {
        if ($this->subject_id == null) return null;
        $exist = self::with('grade')->where('time_id', $this->time_id)->where('teacher_id', $this->teacher_id)->whereNotIn('id', [$this->id])->get();
        return $exist;
        return null;
    }

    public function getOverloadAttribute()
    {
        if ($this->subject_id == null) return false;
        $day_id = Time::where('id', $this->time_id)->first()->day_id;
        $times = Time::where('day_id', $day_id)->get();
        $timeIds = $times->pluck('id');
        $schedules = self::whereIn('time_id', $timeIds)->where('grade_id', $this->grade_id)->where('subject_id', $this->subject_id)->get();
        $subject = Subject::find($this->subject_id);
        $jam = explode(' ', $subject->jam);
        $maxJam = max($jam);
        $totalJamPelajaran = count($schedules);
        if ($totalJamPelajaran > $maxJam) return $totalJamPelajaran - $maxJam;
        return false;
    }

    public static function byGradeId($gradeId)
    {
        return self::join('grades', 'schedules.grade_id', '=', 'grades.id')
            ->join('times', 'schedules.time_id', '=', 'times.id')
            ->with('subject')
            ->select('schedules.*', 'times.jam_mulai', 'times.jam_selesai', 'grades.nama as nama_kelas', 'grades.class_level_id')
            ->where('schedules.grade_id', $gradeId)
            ->get();
    }

    public static function byClassLevel()
    {
        $schedules = [];
        $classLevels = ClassLevel::all();
        foreach ($classLevels as $level) {
            $nama = $level->nama;
            $grades = Grade::where('class_level_id', $level->id)->get();
            foreach ($grades as $key => $grade) {
                $schedules[$nama][$grade->nama] = self::byGradeId($grade->id);
            }
        }
        return $schedules;
    }
}
