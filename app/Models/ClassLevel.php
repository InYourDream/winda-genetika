<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassLevel extends Model
{
    protected $fillable = ['nama', 'kelas', 'major_id'];

    public function grades()
    {
        return $this->hasMany('App\Models\Grade');
    }

    public function major()
    {
        return $this->hasOne('App\Models\Major', 'id', 'major_id');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Models\Subject', 'class_subjects');
    }
}
