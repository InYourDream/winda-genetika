<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{

    protected $fillable = ['jam_mulai', 'jam_selesai', 'day_id'];
    // protected $appends = [''];

    public function day()
    {
        return $this->belongsTo('App\Models\Day');
    }

    public static function fetch($request, $paginate = 10)
    {
        return self::with('day')
            ->where(function ($query) use ($request) {
                if ($request->jam_mulai) $query->where('jam_mulai', 'like', "%$request->jam_mulai%");
                if ($request->jam_selesai) $query->where('jam_selesai', 'like', "%$request->jam_selesai%");
                if ($request->day_id) $query->where('day_id', $request->day_id);
            })
            ->orderBy('day_id')
            ->orderBy('jam_mulai')
            ->paginate($paginate);
    }

    public static function joinDays()
    {
        return self::join('days', 'days.id', '=', 'times.day_id')
            ->select('days.nama as hari', 'times.id as jam_id', 'times.jam_mulai', 'times.jam_selesai', 'day_id as hari_id')
            ->get();
    }

    public function getJamMulaiAttribute($time)
    {
        $time = strtotime($time);
        return date('H:i', $time);
    }

    public function getJamSelesaiAttribute($time)
    {
        $time = strtotime($time);
        return date('H:i', $time);
    }

    public function getRangeAttribute()
    {
        return "$this->jam_mulai - $this->jam_selesai";
    }

    public function getHariAttribute()
    {
        return $this->day->nama;
    }
}
