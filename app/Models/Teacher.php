<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function subjects()
    {
        return $this->belongsToMany('App\Models\Subject', 'grade_subject_teachers', 'teacher_id', 'subject_id');
    }

    public function grades()
    {
        return $this->belongsToMany('App\Models\Grade', 'grade_subject_teachers', 'teacher_id', 'grade_id');
    }

    public function getKelasAttribute()
    {
        return $this->classes->kelas;
    }

    public function getJurusanAttribute()
    {
        return $this->classes->major->nama ;
    }

    public static function fetch($request, $paginate = 10)
    {
        return self::where(function ($query) use ($request) {
                if ($request->nama) $query->where('nama', 'like', "%$request->nama%");
            })
            // ->whereHas('subject', function($query) use ($request) {
            //     if ($request->mata_pelajaran) $query->where('nama', 'like', "%$request->mata_pelajaran%");
            // })
            // ->whereHas('classes', function($query) use ($request) {
            //     if ($request->kelas) $query->where('kelas', "$request->kelas");
            //     if ($request->major_id) $query->where('major_id', "$request->major_id");
            // })
            // ->with('classes', 'classes.major')
            ->orderBy('id')
            ->paginate($paginate);
    }

    public static function task($request, $paginate = 10)
    {
        return self::with('subjects', 'grades')
            ->where(function ($query) use ($request) {
                if ($request->nama) $query->where('nama', 'like', "%$request->nama%");
            })
            ->whereHas('subjects', function($query) use ($request) {
                if ($request->mata_pelajaran) $query->where('nama', 'like', "%$request->mata_pelajaran%");
            })
            ->orderBy('id')
            ->paginate(10);
    }

    public static function fetchByClassId()
    {
        $classLevels = ClassLevel::all();
        foreach ($classLevels as $classLevel) {
            $id = $classLevel->id;
            $teachers = self::whereHas('classes', function($query) use ($id) {
                    $query->where('id', $id);
                })
                ->get();
            $teachersByClass[$id] = $teachers;
        }
        return $teachersByClass;
    }
}
