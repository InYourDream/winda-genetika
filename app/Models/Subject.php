<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Subject extends Model
{
    protected $fillable = ['nama', 'total_jam_pelajaran', 'pertemuan'];
    // protected $appends  = ['jam_pertemuan', 'sisa_pertemuan'];

    public function classes()
    {
        return $this->belongsToMany('App\Models\ClassLevel', 'class_subjects', 'subject_id');
    }

    public static function fetch($request, $paginate = 10)
    {
        return self::where(function ($query) use ($request) {
                if ($request->nama) $query->where('nama', 'like', "%$request->nama%");
            })
            ->whereHas('classes', function($query) use ($request) {
                if ($request->kelas) $query->where('kelas', "$request->kelas");
                if ($request->major_id) $query->where('major_id', "$request->major_id");
            })
            ->with('classes')
            ->orderBy('nama')
            ->paginate($paginate);
    }

    public static function fetchByGradeId($grades = null)
    {
        if ($grades) {
            $grades = Grade::all()->only($grades);
        } else {
            $grades = Grade::all();
        }
        foreach ($grades as $grade) {
            $id = $grade->id;
            $nama_kelas = $grade->nama;
            $subjects = self::fetchJoinWithTeachersAndGrade($id);
            $subjectByGrade[$nama_kelas] = $subjects;
        }
        return collect($subjectByGrade);
    }

    public static function fetchJoinWithTeachersAndGrade($gradeId)
    {
        return self::join('grade_subject_teachers', 'grade_subject_teachers.subject_id', '=', 'subjects.id')
            ->where('grade_subject_teachers.grade_id', $gradeId)
            ->get();
    }

    public static function dieJoinWithTeachersAndClass($classId)
    {
        return self::join('class_subjects', 'class_subjects.subject_id', '=', 'subjects.id')
            ->join('class_levels', 'class_levels.id', '=', 'class_subjects.class_level_id')
            ->join('teachers', 'teachers.subject_id', '=', 'subjects.id')
            ->join('class_teachers', 'class_teachers.teacher_id', '=', 'teachers.id')
            ->select('subjects.nama as pelajaran', 'subjects.id as pelajaran_id', 'subjects.jam', 'subjects.total_jam_pelajaran', 'subjects.pertemuan', 'teachers.nama as guru', 'teachers.id as guru_id')
            ->where('class_levels.id', $classId)
            ->where('class_teachers.class_level_id', $classId)
            ->get();
    }

    public static function total($request)
    {
        return self::where(function ($query) use ($request) {
                if ($request->nama) $query->where('nama', 'like', "%$request->nama%");
            })
            ->whereHas('classes', function($query) use ($request) {
                if ($request->kelas) $query->where('kelas', "$request->kelas");
                if ($request->major_id) $query->where('major_id', "$request->major_id");
            })
            ->select(DB::raw("SUM(pertemuan) as total_pertemuan, SUM(total_jam_pelajaran) as total_jam_pelajaran"))
            ->first();
    }

    public static function createJamKosong()
    {
        $kosong = new self;
        $kosong->pelajaran = 'Jam Kosong';
        $kosong->pelajaran_id = null;
        $kosong->teacher_id = null;
        return $kosong;
    }

    // public function getJamPertemuanAttribute()
    // {
    //     return ceil($this->total_jam_pelajaran / $this->pertemuan);
    // }

    // public function getSisaPertemuanAttribute()
    // {
    //     return $this->total_jam_pelajaran % $this->pertemuan;
    // }
}
