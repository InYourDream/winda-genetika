<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailSubject extends Model
{
    protected $fillable = ['subject_id', 'kelas', 'major_id'];

    function major()
    {
        return $this->belongsTo('App\Models\Major');
    }
}
