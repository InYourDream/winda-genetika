<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Day;
use App\Models\Time;
use function App\Helpers\formatWaktu;

class TimeService
{

    public $timeId;
    public $dayId;

    public $jam_pertama_mulai;
    public $jam_pertama_selesai;
    public $jam_kedua_mulai;
    public $jam_kedua_selesai;

    public function __construct($request, $timeId = null)
    {
        $this->timeId = $timeId;
        $this->dayId = $request->day_id;
        $this->jam_pertama_mulai = formatWaktu($request->jam_mulai);
        $this->jam_pertama_selesai = formatWaktu($request->jam_selesai);
    }

    public function checkBentrok()
    {
        $dayTimes = Day::with('times')->where('id', $this->dayId)->first()->times->except($this->timeId);
        foreach ($dayTimes  as $dayTime) {
            $this->jam_kedua_mulai = formatWaktu($dayTime->jam_mulai);
            $this->jam_kedua_selesai = formatWaktu($dayTime->jam_selesai);
            if ($this->bentrok()) return $dayTime;
        }
    }

    public function bentrok()
    {
        if  (($this->jam_pertama_mulai < $this->jam_kedua_selesai) &&
            ($this->jam_pertama_mulai >= $this->jam_kedua_mulai)) return true;

        if  (($this->jam_pertama_selesai < $this->jam_kedua_mulai) &&
            ($this->jam_pertama_selesai >= $this->jam_kedua_selesai)) return true;

        return false;
    }

    public static function checkValidRange($request)
    {
        if (formatWaktu($request->jam_mulai) < formatWaktu($request->jam_selesai)) return true;
        return false;
    }

    public static function simpanWaktuPadaSemuaHari($request)
    {
        $days = Day::all();
        foreach ($days as $day) {
            $time = new Time;
            $time->jam_mulai = $request->jam_mulai;
            $time->jam_selesai = $request->jam_selesai;
            $time->day_id = $day->id;
            $time->save();
        }
    }

}
