<?php

namespace App\Helpers;

use Carbon\Carbon;

function formatWaktu($value)
{
    return Carbon::createFromFormat('H:i', $value);
}
