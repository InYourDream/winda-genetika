<?php

function checkClass($subject, $classId)
{
    if (!isset($subject)) return null;
    $classes = $subject->classes->pluck('id')->toArray();
    if (in_array($classId, $classes)) return 'checked';
    return null;
}

function checkList($mapel, $i, $data, $teacher, $grade) {
    if (!isset($mapel[$i])) return;
    if (!isset($data[$teacher->id][$mapel[$i]['id']][$grade->id])) return;
    return $mapel[$i]['jam'];
    return '<span class="fa fa-check"></span>';
}

