<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'day_id' => 'required',
            'time_id' => 'required',
            'grade_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'day_id.required' => 'Hari wajib di isi !!',
            'time_id.required' => 'Jam wajib di isi !!',
            'grade_id.required' => 'Kelas wajib di isi !!',
        ];
    }

}
