<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'jam' => 'required|max:4|min:1',
            'classes' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Nama mata pelajaran wajib di isi !!',
            'jam.required' => 'Jam Pelajaran / Pertemuan wajib di isi !!',
            'jam.max' => 'Maaf tidak bisa melebih 4 jam pelajaran',
            'jam.min' => 'Maaf tidak bisa kurang 1 jam pelajaran',
            'classes.required' => 'Kelas wajib di isi !!',
        ];
    }
}
