<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GradeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'kelas' => 'required',
            'major_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => 'Nama kelas wajib di isi !!',
            'kelas.required' => 'Kelas wajib di isi !!',
            'major_id.required' => 'Jurusan wajib di isi !!',
        ];
    }
}
