<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jam_mulai' => 'required|date_format:H:i',
            'jam_selesai' => 'required|date_format:H:i',
        ];
    }

    public function messages()
    {
        return [
            'jam_mulai.required' => 'Jam mulai wajib di isi !!',
            'jam_mulai.date_format' => 'Format jam yang dipakai hh:mm, contoh: 07:15',
            'jam_selesai.required' => 'Jam selesai wajib di isi !!',
            'jam_selesai.date_format' => 'Format jam yang dipakai hh:mm, contoh: 07:15',
        ];
    }
}
