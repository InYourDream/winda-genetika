<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use App\Models\ClassLevel;
use App\Http\Requests\SubjectRequest;

class SubjectController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $subjects = Subject::fetch($request);
        $classLevels = ClassLevel::all();
        $total = Subject::total($request);
        $url = url()->full();
        session()->put('url_pelajaran', $url);
        return view('app.pelajaran.index', compact('subjects', 'total', 'classLevels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classes = ClassLevel::all();
        return view('app.pelajaran.create', compact('classes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectRequest $request)
    {
        $jam = explode(' ', $request->jam);

        $subject = new Subject;
        $subject->nama = $request->nama;
        $subject->jam = $request->jam;
        $subject->total_jam_pelajaran = array_sum($jam);
        $subject->pertemuan = count($jam);
        $subject->save();
        $subject->classes()->sync($request->classes);

        return redirect()->back()->with(['pesan' => __('messages.simpan'), 'store' => $subject]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = Subject::with('classes')->find($id);
        foreach ($subject->classes as $class) {
            $select[] = [
                'id' => $class->id,
                'text' => $class->nama,
            ];
        }
        return response()->json($select);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::with('classes')->find($id);
        $classes = ClassLevel::all();

        return view('app.pelajaran.edit', compact('subject', 'classes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SubjectRequest $request, $id)
    {
        $jam = explode(' ', $request->jam);
        $subject = Subject::find($id);
        $subject->nama = $request->nama;
        $subject->jam = $request->jam;
        $subject->total_jam_pelajaran = array_sum($jam);
        $subject->pertemuan = count($jam);
        $subject->classes()->sync($request->classes);
        $subject->save();

        return redirect(session('url_pelajaran'))->with(['pesan' => __('messages.update'), 'updated_id' => $subject->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subject::find($id)->delete();
        return redirect(session('url_pelajaran'))->with(['pesan' => __('messages.hapus')]);
    }
}
