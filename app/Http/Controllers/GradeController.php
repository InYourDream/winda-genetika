<?php

namespace App\Http\Controllers;

use App\Models\Grade;
use Illuminate\Http\Request;
use App\Http\Requests\GradeRequest;
use App\Models\ClassLevel;

class GradeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $grades = Grade::fetch($request);
        $url = url()->full();
        session()->put('url_kelas', $url);
        return view('app.kelas.index', compact('grades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.kelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GradeRequest $request)
    {
        $classLevel = ClassLevel::where('kelas', $request->kelas)->where('major_id', $request->major_id)->first();
        $grade = new Grade;
        $grade->nama = $request->nama;
        $grade->class_level_id = $classLevel->id;
        $grade->save();
        return redirect()->back()->with(['pesan' => __('messages.simpan'), 'store' => $request->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grade = Grade::find($id);
        return view('app.kelas.edit', compact('grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(gradeRequest $request, $id)
    {
        $classLevel = ClassLevel::where('kelas', $request->kelas)->where('major_id', $request->major_id)->first();
        $grade = grade::find($id);
        $grade->nama = $request->nama;
        $grade->class_level_id = $classLevel->id;
        $grade->save();
        return redirect(session('url_kelas'))->with(['pesan' => __('messages.update'), 'updated_id' => $grade->id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        grade::find($id)->delete();
        return redirect(session('url_kelas'))->with(['pesan' => __('messages.hapus')]);
    }
}

