<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Http\Requests\TeacherRequest;
use App\Models\Grade;
use App\Models\ClassLevel;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $teachers = Teacher::fetch($request);
        $url = url()->full();
        $grades = Grade::with('class')->get();
        $classLevels = $grades->groupBy('class_level_id');
        session()->put('url_guru', $url);
        return view('app.guru.index', compact('teachers', 'grades', 'classLevels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.guru.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherRequest $request)
    {
        $teacher = new Teacher;
        $teacher->id = $request->id;
        $teacher->nama = $request->nama;
        $teacher->save();
        return redirect()->back()->with(['pesan' => __('messages.simpan'), 'store' => $teacher]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = Teacher::find($id);
        return view('app.guru.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherRequest $request, $id)
    {
        $teacher = Teacher::find($id);
        $teacher->nama = $request->nama;
        $teacher->save();

        return redirect(session('url_guru'))->with(['pesan' => __('messages.update'), 'updated_id' => $teacher->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::find($id)->delete();
        return redirect(session('url_guru'))->with(['pesan' => __('messages.hapus')]);
    }
}
