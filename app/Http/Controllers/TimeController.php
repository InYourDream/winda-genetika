<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TimeRequest;
use App\Services\TimeService;
use App\Models\Time;
use App\Models\Day;

class TimeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $days = Day::all();
        $times = Time::fetch($request);
        $url = url()->full();
        session()->put('url_waktu', $url);
        return view('app.waktu.index', compact('days', 'times'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $days = Day::all();
        return view('app.waktu.create', compact('days'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TimeRequest $request)
    {
        $validasi = TimeService::checkValidRange($request);
        if (!$validasi) {
            $error = __('messages.waktu tidak valid', $request->toArray());
            return redirect()->back()->withInput()->withErrors($error);
        };

        $timeService = new TimeService($request);
        if ($dayTime = $timeService->checkBentrok()) {
            $error =  __('messages.waktu kebentrok', $dayTime->toArray());
            return redirect()->back()->withInput()->withErrors($error);
        }

        if ($request->day_id == null) {
            TimeService::simpanWaktuPadaSemuaHari($request);
            return redirect()->back()->with(['pesan' => __('messages.simpan') ]);
        }

        $time = Time::create($request->all());
        return redirect()->back()->with(['pesan' => __('messages.simpan'), 'store' => $time]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Time  $time
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Time  $time
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $days = Day::all();
        $time = Time::find($id);
        return view('app.waktu.edit', compact('days', 'time'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TimeRequest $request, $id)
    {
        $validasi = TimeService::checkValidRange($request);
        if (!$validasi) {
            $error = __('messages.waktu tidak valid', $request->toArray());
            return redirect()->back()->withInput()->withErrors($error);
        };

        $time = Time::find($id);
        $timeService = new TimeService($request, $time->id);
        if ($dayTime = $timeService->checkBentrok()) {
            $error =  __('messages.waktu kebentrok', $dayTime->toArray());
            return redirect()->back()->withInput()->withErrors($error);
        }

        $time->update($request->all());
        return redirect(session('url_waktu'))->with(['pesan' => __('messages.update'), 'updated_id' => $time->id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Time::find($id)->delete();
        return redirect(session('url_waktu'))->with(['pesan' => __('messages.hapus')]);
    }
}
