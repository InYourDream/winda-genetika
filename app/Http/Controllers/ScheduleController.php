<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\Day;
use App\Models\ClassLevel;
use App\Http\Requests\ScheduleRequest;
use App\Models\Time;
use Illuminate\Support\Carbon;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $days = Day::all();
        $classLevels = ClassLevel::all();
        $schedules = Schedule::fetch($request, 25);
        $url = url()->full();
        session()->put('url_jadwal', $url);
        return view('app.jadwal.index', compact('schedules', 'days', 'classLevels'));
    }

    public function preview()
    {
        $times = Time::all();
        $schedulesByClass = Schedule::byClassLevel();
        return view('app.jadwal.preview', compact('schedulesByClass', 'times'));
    }

    public function cetak()
    {
        setlocale(LC_ALL, 'id_ID.utf8');
        Carbon::setLocale('id_ID.utf8');
        $days = Day::with('times')->get();
        $schedulesByClass = Schedule::byClassLevel();
        return view('app.jadwal.cetak', compact('schedulesByClass', 'days'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $days = Day::all();
        $classLevels = ClassLevel::all();

        return view('app.jadwal.create', compact('classLevels', 'days'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScheduleRequest $request)
    {
        $validasi = Schedule::where('time_id', $request->time_id)->where('grade_id', $request->grade_id)->first();
        if ($validasi) {
            $error = __('messages.jadwal sudah ada', $request->toArray());
            return redirect()->back()->withInput()->withErrors($error);
        }

        $schedule = new Schedule;
        $schedule->time_id = $request->time_id;
        $schedule->subject_id = $request->subject_id;
        $schedule->grade_id = $request->grade_id;
        $schedule->teacher_id = $request->teacher_id;
        $schedule->save();

        return redirect()->back()->with(['pesan' => __('messages.simpan'), 'store' => $schedule]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $days = Day::all();
        $classLevels = ClassLevel::all();
        $schedule = Schedule::find($id);

        return view('app.jadwal.edit', compact('schedule', 'classLevels', 'days'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ScheduleRequest $request, $id)
    {
        $schedule = Schedule::find($id);
        if ($schedule->time_id != $request->time_id) {
            $validasi = Schedule::where('time_id', $request->time_id)->where('grade_id', $request->grade_id)->first();
            if ($validasi) {
                $error = __('messages.jadwal sudah ada', $request->toArray());
                return redirect()->back()->withInput()->withErrors($error);
            }
        }

        $schedule->time_id = $request->time_id;
        $schedule->subject_id = $request->subject_id;
        $schedule->grade_id = $request->grade_id;
        $schedule->teacher_id = $request->teacher_id;
        if ($request->kosong) $schedule->subject_id = $schedule->teacher_id = null;

        $schedule->save();
        return redirect(session('url_jadwal'))->with(['pesan' => __('messages.simpan'), 'store' => $schedule]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Schedule::find($id)->delete();
        return redirect(session('url_jadwal'))->with(['pesan' => __('messages.hapus')]);
    }
}
