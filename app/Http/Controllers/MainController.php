<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;

class mainController extends Controller
{
    public function home()
    {
        $totalTime = DB::table('times')->count();
        $totalClass = DB::table('grades')->count();
        $totalSubject = DB::table('subjects')->count();
        $totalTeacher = DB::table('teachers')->count();
        $totalSchedule = DB::table('schedules')->count();
        $total = [
            'time' => $totalTime,
            'class' => $totalClass,
            'subject' => $totalSubject,
            'teacher' => $totalTeacher,
            'schedule' => $totalSchedule,
        ];
        return view('home.index', compact('total'));
    }

    public function changePassword()
    {
        return view('auth/ubah-password');
    }

    public function updatePassword(Request $request)
    {
        $messages = [
            'password.required' => 'password wajib di isi',
            'password.min' => 'minimal password 3 karakter',
            'password.confirmed' => 'password harus sama',
            'new_password.min' => 'minimal password 3 karakter',
            'new_password.confirmed' => 'password mesti sama',
        ];

        $request->validate([
            'password' => 'required|string|min:3',
            'new_password' => 'required|string|min:3|confirmed',
        ], $messages);

        $user = auth()->user();
        $confirm = Hash::check($request->password, $user->password);
        if (!$confirm) {
            $error = 'Password anda salah !';
            return redirect()->back()->withInput()->withErrors($error);
        }
        $user->password = bcrypt($request->new_password);
        $user->save();
        return redirect()->back()->with('pesan', 'Password berhasil diubah !!');
    }
}
