<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Day;
use App\Models\Subject;
use App\Models\ClassLevel;
use App\Models\Teacher;
use App\Models\Grade;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function day($id)
    {
        $day = Day::with('times')->where('id', $id)->first();
        $select = [];
        foreach ($day->times as $time) {
            $select[] = [
                'id' => $time->id,
                'text' => $time->jam_mulai . ' - ' . $time->jam_selesai,
            ];
        }
        return response()->json($select);
    }

    public function grade($classLevelId)
    {
        $grades = Grade::where('class_level_id', $classLevelId)->get();
        $select = [];
        foreach ($grades as $grade) {
            $select[] = [
                'id' => $grade->id,
                'text' => $grade->nama,
            ];
        }
        return response()->json($select);
    }

    public function subjectByClass($id)
    {
        $classLevel = ClassLevel::with('subjects')->where('id', $id)->first();
        $select = [];
        foreach ($classLevel->subjects as $subject) {
            $select[] = [
                'id' => $subject->id,
                'text' => $subject->nama,
            ];
        }
        return response()->json($select);
    }

    public function teacher($subjectId, $classLevelId)
    {
        $teacher = Teacher::where('subject_id', $subjectId)
            ->whereHas('classes', function($query) use ($classLevelId) {
                if ($classLevelId) $query->where('id', $classLevelId);
            })
            ->first();
        return response()->json($teacher);
    }

    public function TaskTeacher($id)
    {
        $gradeId = DB::table('grade_subject_teachers')->where('teacher_id', $id)->get()->pluck('grade_id');
        return response()->json($gradeId);
    }
}
