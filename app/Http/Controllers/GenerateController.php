<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Algorithms\GeneticAlgorithms\Main as GeneticAlgorithms;
use App\Models\Schedule;
use App\Models\Grade;

class GenerateController extends Controller
{
    public function index()
    {
        return view('app.generate.index');
    }

    public function generate(Request $request)
    {
        $genetic = new GeneticAlgorithms();
        $genetic->populationLength = $request->population_length;
        $genetic->crossoverRate =  $request->crossover_rate;
        $genetic->mutationRate =  $request->mutation_rate;
        $genetic->maxGeneration =  $request->max_generation;
        $pesan = $genetic->validate();
        if ($pesan) {
            $error = $pesan;
            return redirect()->back()->withInput()->withErrors($error);
        }
        [$solution, $generation] = $genetic->solution();
        // return view('app.generate.hasil', compact('solution', 'generation'));
    }

    public function hasilGenerate($solution, $generation)
    {
        return view('app.generate.hasil', compact('solution', 'generation'));
    }

    public function store(Request $request)
    {
        if (!$request->all()) return redirect()->back();
        Schedule::truncate();
        foreach ($request->time_id as $key => $time_id) {
            $schedule = new Schedule;
            $schedule->time_id = $time_id;
            $schedule->subject_id = $request->subject_id[$key];
            $schedule->teacher_id = $request->teacher_id[$key];
            $schedule->grade_id = $request->grade_id[$key];
            $schedule->save();
        }
        return redirect()->route('jadwal.index')->with(['pesan' => 'Hasil generate jadwal berhasil di simpan']);;
    }
}
