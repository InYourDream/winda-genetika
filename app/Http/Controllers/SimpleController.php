<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Algorithms\SimpleGeneticAlgorithms\Main as GeneticAlgorithms;

class SimpleController extends Controller
{
    public function simpleGeneticAlgorithms()
    {
        $genetic = new GeneticAlgorithms();
        $genetic->target = 'laravel 5.7';
        $genetic->delay = 10;
        $genetic->LengthPopulation = 5;
        $genetic->solution();
    }
}
