<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Models\Subject;
use App\Http\Requests\TeacherRequest;
use App\Models\Grade;
use App\Models\GradeSubjectTeacher;
use App\Models\ClassLevel;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\TaskRequest;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $counts = DB::table('grade_subject_teachers')
            ->join('subjects', 'grade_subject_teachers.subject_id', '=', 'subjects.id')
            ->get();

        foreach ($counts as $value) {
            if (!isset($total[$value->grade_id])) {
                $total[$value->grade_id] = $value->total_jam_pelajaran;
            } else {
                $total[$value->grade_id] += $value->total_jam_pelajaran;
            }
        }

        $teachers = Teacher::task($request);
        $teacherIds = $teachers->pluck('id')->toArray();
        $tasks = DB::table('grade_subject_teachers')->whereIn('teacher_id', $teacherIds)->get();
        $data = [];
        foreach ($tasks as $task) {
            $data[$task->teacher_id][$task->subject_id][$task->grade_id] = true;
        }

        $url = url()->full();
        $grades = Grade::with('class')->get();
        $classLevels = $grades->groupBy('class_level_id');
        session()->put('url_tugas', $url);
        return view('app.tugas.index', compact('teachers', 'grades', 'classLevels', 'data', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        $teachers = Teacher::all();
        $subjects = Subject::orderBy('nama')->get();
        $classLevels = ClassLevel::with('grades')->get();
        return view('app.tugas.create', compact('subjects', 'teachers', 'classLevels', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequest $request)
    {
        $teacher = Teacher::find($request->teacher_id);
        $attached = DB::table('grade_subject_teachers')->where('teacher_id', $request->teacher_id)->get()->pluck('grade_id');

        $grades = array_diff($request->grade_id ?? [], $attached->toArray());
        $subject = array_fill(0, count($grades), ['subject_id' => $request->subject_id, 'teacher_id' => $request->teacher_id]);
        $data  = array_combine($grades, $subject);
        $teacher->grades()->attach($data);

        return redirect()->back()->with(['pesan' => __('messages.simpan'), 'store' => null]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($teacher_id, $subject_id)
    {
        $tasks = DB::table('grade_subject_teachers')->where('teacher_id', $teacher_id)->get()->toArray();
        $selected = [];
        $notSelected = [];
        foreach ($tasks as $task) {
            if ($task->subject_id == $subject_id) {
                $selected[] = $task->grade_id;
            } else {
                $notSelected[] = $task->grade_id;
            }
        }

        $teachers = Teacher::all();
        $subjects = Subject::orderBy('nama')->get();
        $classLevels = ClassLevel::with('grades')->get();
        return view('app.tugas.edit', compact('subjects', 'teacher', 'teachers', 'classLevels', 'teacher_id', 'subject_id', 'selected', 'notSelected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(TaskRequest $request)
    {
        $teacher = Teacher::find($request->teacher_id);
        DB::table('grade_subject_teachers')->where('teacher_id', $request->teacher_id)->where('subject_id', $request->subject_id)->delete();

        $grades = $request->grade_id ?? [];
        $subject = array_fill(0, count($grades), ['subject_id' => $request->subject_id, 'teacher_id' => $request->teacher_id]);
        $data  = array_combine($grades, $subject);
        $teacher->grades()->attach($data);

        return redirect(session('url_tugas'))->with(['pesan' => __('messages.update'), 'updated_id' => $teacher->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($teacher_id, $subject_id)
    {
        $task = DB::table('grade_subject_teachers')->where('teacher_id', $teacher_id)->where('subject_id', $subject_id)->delete();
        return redirect(session('url_tugas'))->with(['pesan' => __('messages.hapus')]);
    }
}
